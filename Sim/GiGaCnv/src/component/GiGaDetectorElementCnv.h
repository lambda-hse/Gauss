#ifndef    GIGA_GIGADETECTORELEMENT_H
#define    GIGA_GIGADETECTORELEMENT_H 1

// Include files
// from GiGaCnv
#include "GiGaCnv/GiGaCnvBase.h"
#include "GiGaCnv/GiGaLeaf.h"

/** @class GiGaDetectorElementCnv GiGaDetectorElementCnv.h 
 *
 *  converter of DetectorElements into Geant4 Geometry tree
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 */

class GiGaDetectorElementCnv: public GiGaCnvBase
{
public:

  /** create the representation
   *  @param Object  pointer to data object
   *  @param Address address
   *  @return status code
   */
  StatusCode createRep
  ( DataObject*     Object  ,
    IOpaqueAddress*& Address ) override;

  /** update the  representation
   *  @param Address address
   *  @param Object  pointer to data object
   *  @return status code
   */
  StatusCode updateRep
  ( IOpaqueAddress*  Address,
    DataObject*      Object   ) override;

  /// class ID for converted objects
  static const CLID&         classID();

  /// storage Type
  static unsigned char storageType() ;
  ///

  /** standard constructor 
   *  @param svc pointer to Service Locator 
   */
  GiGaDetectorElementCnv( ISvcLocator* svc );

  /// virtual destructor
  virtual ~GiGaDetectorElementCnv();

private:

  GiGaLeaf  m_leaf;

};


#endif  // GIGA_GIGADETECTORELEMENT_H
// ============================================================================
