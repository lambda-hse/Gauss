#
# Options specific for a given job
# ie. setting of random number seed and name of output files
#
from GaudiKernel.ProcessJobOptions import importOptions
from Gaudi.Configuration import *
from Configurables import Gauss, ApplicationMgr, UpdateManagerSvc
from Gauss.Configuration import *    

#importOptions("$APPCONFIGOPTS/Gauss/Sim08-Beam3500GeV-md100-2011-nu2.py")


#importOptions("/afs/cern.ch/lhcb/software/releases/GAUSS/GAUSS_v48r3/Sim/Gauss/options/Gauss-2012.py")
#importOptions("$APPCONFIGOPTS/Gauss/Sim08-Beam4000GeV-mu100-2012-nu2.5.py")
from Configurables import LHCbApp
#LHCbApp().DDDBtag   = "dddb-20170721-3"
#LHCbApp().CondDBtag = "sim-20170721-2-vc-md100"



#EventSelector().Input = ["DATAFILE='00012345_00006789_0_Trigger_Brunelv43r2p12.xdst' SVC='Gaudi::RootEvtSelector' OPT='READ'"]
importOptions("$LBDELPHESROOT/options/GbDelphes.py")

            
#--Number of events
nEvts = 1000
LHCbApp().EvtMax = nEvts

#from Configurables import FakeEventTime, EventClockSvc
#EventClockSvc().EventTimeDecoder = 'FakeEventTime'

#--Generator phase, set random numbers
#gaussGen = GenInit("GaussGen")
#gaussGen.FirstEventNumber = 1
#gaussGen.RunNumber        = 1082

#genMonitor = GaudiSequencer( "GenMonitor" )
#genMonitor.Members += [ "DumpHepMCTree/DumpSignal", "DumpHepMC/DumpAll"]
#delphesMonitor = GaudiSequencer( "DelphesMonitor" )
#delphesMonitor.Members += [ "PrintMCDecayTreeAlg/PrintMCInputDelphes",  "PrintMCDecayTreeAlg/PrintMCOutputDelphes" ]
#from Configurables import PrintMCDecayTreeAlg
#PrintMCDecayTreeAlg("PrintMCOutputDelphes").MCParticleLocation = "MCFast/MCParticles"
#PrintMCDecayTreeAlg("PrintMCOutputDelphes").MCParticleLocation = "Rec/Track/Best"
#PrintMCDecayTreeAlg("PrintMCOutputDelphes").MCParticleLocation = "Rec/ProtoP/Charged"

eventType = '30000000'
#--Set name of output files for given job and read in options
idFile = 'Gauss_'+str(eventType)
HistogramPersistencySvc().OutputFile = idFile+'_histos.root'
#--- Save ntuple with hadronic cross section information
#ApplicationMgr().ExtSvc += [ "NTupleSvc" ]
NTupleSvc().Output = ["FILE1 DATAFILE='GaussTuple.root' TYP='ROOT' OPT='NEW'"]
       
#FileCatalog().Catalogs = [ "xmlcatalog_file:Gauss_NewCatalog_0.xml" ]

#importOptions("/afs/cern.ch/lhcb/software/releases/LHCB/LHCB_v38r7/Det/DetDescChecks/options/LoadDDDB.py")

# Output setup
#tape = OutputStream("GaussTape")
#tape.Output = "DATAFILE='PFN:myOutputFile.sim' TYP='POOL_ROOTTREE' OPT='RECREATE'"
#ApplicationMgr( OutStream = [tape] )
OutputStream("GaussTape").Output = "DATAFILE='PFN:myOutputFile.sim' TYP='POOL_ROOTTREE' OPT='RECREATE'"

def writeHits():
    OutputStream("GaussTape").ItemList += ["/Event/MC/Vertices#1"]
    OutputStream("GaussTape").ItemList += ["/Event/MC/Particles#1"]
    OutputStream("GaussTape").ItemList += ["/Event/MCFast/MCParticles#1"]
    OutputStream("GaussTape").ItemList += ["/Event/MCFast/MCVertices#1"]
#    OutputStream("GaussTape").ItemList += ["/Event/Rec/ProtoP/Charged#1"]
#    OutputStream("GaussTape").ItemList += ["/Event/Rec/Track/Best#1"]
#    OutputStream("GaussTape").ItemList += ["/Event/Rec/Rich/PIDs#1"]
#    OutputStream("GaussTape").ItemList += ["/Event/MC/DigiHeader#1"]
#    OutputStream("GaussTape").ItemList += ["/Event/DAQ/RawEvent#1"]
#    OutputStream("GaussTape").ItemList += ["/Event/DAQ/ODIN#1"]
appendPostConfigAction(writeHits)

