#!/bin/bash

DATE=$(date +"%d_%m_%Y_run")
N=1

# Increment $N as long as a directory with that name exists
while [[ -d "$DATE$N" ]] ; do
    N=$(($N+1))
done

mkdir "$DATE$N"
cp Gauss-Job-New.py LbDelphes.py card.tcl geometry_data.txt $DATE$N/
cd "$DATE$N"
. /cvmfs/lhcb.cern.ch/lib/lhcb/LBSCRIPTS/LBSCRIPTS_v9r2p7/InstallArea/scripts/LbLogin.sh -c x86_64-centos7-gcc7-opt
../../../../build.x86_64-centos7-gcc7-opt/run gaudirun.py Gauss-Job-New.py | tee log.txt
