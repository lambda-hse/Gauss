## 
##  Example on how to run only the generator phase
##  It can be passed as additional argument to gaudirun.py directly
##   > gaudirun.py $APPCONFIGOPTS/Gauss/MC09-b5TeV-md100.py \
##                 $APPCONFIGOPTS/Conditions/MC09-20090602-vc-md100.py \
##                 $DECFILESROOT/options/EVENTTYPE.opts \
##                 $LBPYTHIAROOT/options/Pythia.opts \
##                 $GAUSSOPTS/GenStandAlone.py \
##                 $GAUSSOPTS/Gauss-Job.py
##  or you can set the property in your Gauss-Job.py
##  Port to python of GenStandAlone.opts
## 

from Configurables import Gauss
from Gaudi.Configuration import * 
import os
Gauss().Phases = ["Generator"]

def delphesForGauss():
    from Configurables import (GaudiSequencer, SimInit, DelphesAlg,
                               ApplicationMgr, DelphesHist, DelphesTuple,
                               DelphesProto, BooleInit)

    #DelphesAlg().LHCbDelphesCardLocation = '$LBDELPHESROOT/options/cards/delphes_card_LHCb.tcl'
#    DelphesAlg().LHCbDelphesCardLocation = '$LBDELPHESROOT/options/cards/delphes_card_LHCb_withEcal.tcl'
#    DelphesAlg().LHCbDelphesCardLocation = '$LBDELPHESROOT/options/cards/delphes_card_LHCb_DoubleStep_Original.tcl'
    DelphesAlg().LHCbDelphesCardLocation = '$LBDELPHESROOT/options/cards/delphes_card_LHCb_Efficiency.tcl'
#    DelphesAlg().DelphesTrackLocation = 'FinalTrackMerger/tracks'
#    DelphesAlg().DelphesTrackLocation = 'TrackMergerEndRich2/tracks'
    DelphesAlg().DelphesTrackLocation = 'TrackMergerEndVelo/tracks'
#    DelphesProto().LHCbDelphesCardLocation = '$LBDELPHESROOT/options/cards/delphes_card_LHCb_withEcal.tcl'
    
    delphesSeq = GaudiSequencer("DelphesSeq")
    delphesSeq.Members = [ SimInit("InitDelphes") ]
    delphesSeq.Members += [ DelphesAlg("DelphesAlg") ]
    delphesSeq.Members += [ DelphesHist("DelphesHist") ]
    delphesSeq.Members += [ DelphesTuple("DelphesTuple") ]
#    delphesSeq.Members += [ DelphesProto("DelphesProto") ]
#    delphesSeq.Members += [ BooleInit("BooleInit", ModifyOdin = True) ]
    delphesSeq.Members += [ GaudiSequencer("DelphesMonitor") ]
    from Configurables import GaudiSequencer
    GaudiSequencer('GaussSequencer').Members = []
    ApplicationMgr().EvtSel = ""
    ApplicationMgr().TopAlg += [ delphesSeq ]

appendPostConfigAction( delphesForGauss )

