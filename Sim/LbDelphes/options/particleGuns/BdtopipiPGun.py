# content of the file BParticleGun.py
#pgun.FlatNParticles.MinNParticles = 2
#pgun.FlatNParticles.MaxNParticles = 2
from Configurables import ParticleGun
from Configurables import MomentumRange
ParticleGun().addTool( MomentumRange )
from GaudiKernel import SystemOfUnits
ParticleGun().MomentumRange.MomentumMin = 1.0*SystemOfUnits.GeV
from GaudiKernel import SystemOfUnits
ParticleGun().MomentumRange.MomentumMax = 100.*SystemOfUnits.GeV
ParticleGun().EventType = 11102013
#ParticleGun().EventType = 34102100
ParticleGun().GenCutTool = "DaughtersInLHCb"
ParticleGun().ParticleGunTool = "MomentumRange"
ParticleGun().NumberOfParticlesTool = "FlatNParticles"
ParticleGun().MomentumRange.PdgCodes = [ -511, 511 ]
ParticleGun().SignalPdgCode = 511
from Configurables import ToolSvc
from Configurables import EvtGenDecay
tsvc = ToolSvc()
tsvc.addTool( EvtGenDecay , name = "EvtGenDecay" )
#tsvc.EvtGenDecay.UserDecayFile = "/cvmfs/lhcb.cern.ch/lib/lhcb/DBASE/Gen/DecFiles/v30r18/dkfiles/D0_pipi=DecProdCut.dec"
tsvc.EvtGenDecay.UserDecayFile = "$DECFILESROOT/dkfiles/Bd_pi+pi-=CPV,DecProdCut.dec"
#tsvc.EvtGenDecay.UserDecayFile = "$DECFILESROOT/dkfiles/Ks_pipi.dec"
ParticleGun().DecayTool = "EvtGenDecay"

from Gaudi.Configuration import *
importOptions( "$DECFILESROOT/options/CaloAcceptance.py" )



