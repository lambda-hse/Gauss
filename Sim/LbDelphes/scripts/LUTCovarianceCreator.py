#!/usr/local/bin/ipython%

import ROOT, sys, os
from ROOT import TTree, TFile, TH1F, TH2F, TH1D, TH2D, TCanvas, TProfile, TAxis
import numpy as np


def LUTCreatorList(histos, fout):
    nbins = histos[0].GetNbinsX()
    fOut = open(fout,"w")
    for ibin in range(1,nbins):
        xmin = histos[0].GetBinLowEdge(ibin)
        xmax = histos[0].GetBinLowEdge(ibin) + histos[0].GetBinWidth(ibin)
        entry = "{0}\t{1}".format(xmin,xmax)
        for i in histos:
            mean = i.GetBinContent(ibin)
            err  = i.GetBinError(ibin)
            entry += "\t{0}\t{1}".format(mean,err)            
        entry += "\n"

        fOut.write(entry)
    fOut.close()
        

file = TFile("trackRes_BtoHH_RecCat.root","OPEN")
trackTree  = file.Get("TrackEffRes/TrackTree")

nbinsP = 100
nbinsCov = 100

covProf = [0 for x in range(25)]    
        
for i in range(25):
    covProf[i] = TProfile("covProf{0}".format(i),"covProf{0}".format(i),nbinsP,0,0.001)#"s"
    for event in trackTree:
        covMtx = trackTree.covAtVelo
        covProf[i].Fill(1./trackTree.p_velo,covMtx[i],1)

LUTCreatorList(covProf,"lutCovarianceProf.dat")
