#!/usr/local/bin/ipython

import ROOT, sys, os
from ROOT import TTree, TFile, TH1F, TH2F, TCanvas, TProfile, TAxis
#from ROOT import *

def LUTCreatorList(histos, fout):
    nbins = histos[0].GetNbinsX()
    fOut = open(fout,"w")
    for ibin in range(1,nbins):
        xmin = histos[0].GetBinLowEdge(ibin)
        xmax = histos[0].GetBinLowEdge(ibin) + histos[0].GetBinWidth(ibin)
        entry = "{0}\t{1}".format(xmin,xmax)
        for i in histos:
            mean = i.GetBinContent(ibin)
            err  = i.GetBinError(ibin)
            entry += "\t{0}\t{1}".format(mean,err)            
        entry += "\n"

        fOut.write(entry)
    fOut.close()
        
    
#file = TFile("trackRes.root","OPEN")
file = TFile("trackRes_BtoHH_RecCat.root","OPEN")
trackTree  = file.Get("TrackEffRes/TrackTree")

nbinsP = 100
nbinsGP = 100
nbinsChi2 = 100
nbinsnDoF = 100
nbinslikelihood = 100

gPOneOverP = TProfile("gPOneOverP","gPOneOverP",nbinsP,0,0.001)
chi2OneOverP = TProfile("chi2OneOverP","chi2OneOverP",nbinsP,0,0.001)
nDoF = TProfile("nDoF","nDoF",nbinsP,0,0.001)
likelihood = TProfile("likelihood","likelihood",nbinsP,0,0.001)
gPOneOverP.Sumw2()
chi2OneOverP.Sumw2()
nDoF.Sumw2()
likelihood.Sumw2()

for event in trackTree:
    gPOneOverP.Fill(1./trackTree.p_velo, trackTree.ghostProb,1)
    chi2OneOverP.Fill(1./trackTree.p_velo,trackTree.chi2PerDoF,1)
    nDoF.Fill(1./trackTree.p_velo,trackTree.nDoF,1)
    likelihood.Fill(1./trackTree.p_velo,trackTree.likelihood,1)


LUTCreatorList([chi2OneOverP,nDoF,gPOneOverP,likelihood],"lutTrack.dat")
