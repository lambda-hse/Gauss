#!/usr/bin/python
import ROOT
from ROOT import TH1F, TH2F, TH3F, TH3D, TTree, TFile, TCanvas, TCut, THnSparseD
import numpy as np

fileMBias = TFile("../trackRes_BtoHH_RecCat.root","READ")

treeMBias_mc      = fileMBias.Get("TrackEffRes/TrackTree_mc")
treeMBias_recoble = fileMBias.Get("TrackEffRes/TrackTree_reconstructible")
treeMBias_recoed  = fileMBias.Get("TrackEffRes/TrackTree")


cut_txtyvelo = "!(((abs(tx_velo)>300e-3) || (abs(ty_velo)>250e-3)) || ((abs(tx_velo)<12e-3) && (abs(ty_velo)<12e-3)))"
cut_txtymc   = "!(((abs(mc_tx)>300e-3) || (abs(mc_ty)>250e-3)) || ((abs(mc_tx)<12e-3) && (abs(mc_ty)<12e-3)))"

zcut     = "((mc_z<800) && (mc_z>-500))"
IDcut    = "(abs(mcID)==211)"
IDmothercut    = "(abs(mcIDmother)==511)"
#vtxcut   = "(mc_vertexType==1 || mc_vertexType==2)"
vtxcut   = "(mc_vertexType==2)"
#vtxcut   = "(mc_vertexType==1)"
#rcut     = "(mc_r<0.5)"
rcut     = "(mc_r<150)"
#oneOpcut = "((1./mc_p < 0.001)&&(1./mc_p > 10e-6))"
oneOpcut = "((1./p_velo < 0.001)&&(1./p_velo > 10e-6))"

total_cut = ""
#total_cut += cut_txtyvelo
total_cut += cut_txtymc
total_cut += " && "
total_cut += zcut
#total_cut += " && "
#total_cut += IDcut
#total_cut += " && "
#total_cut += IDmothercut
total_cut += " && "
total_cut += vtxcut
#total_cut += " && "
#total_cut += rcut
#total_cut += " && "
#total_cut += oneOpcut

nbinsX = 100
nbinsY = 100
nbinsZ = 500
xmin   = -1.0
xmax   = 1.0
ymin   = -0.3
ymax   = 0.3
zmin   = 0.0
zmax   = 0.001

nbins = np.array([nbinsX, nbinsY, nbinsZ, 500, 500, 500], dtype=np.int32)
xminv = np.array([xmin, ymin, 0, -0.02, -0.02, -2.5e3]) 
xmaxv = np.array([xmax, ymax, 100e3, 0.02, 0.02, 2.5e3])


sparseRes_770 = THnSparseD("sparseRes_770","sparseRes_770",6,nbins,xminv,xmaxv)
sparseRes_9400 = THnSparseD("sparseRes_9400","sparseRes_9400",6,nbins,xminv,xmaxv)
hReconstructible_770 = TH3D("hReconstructible_770","hReconstructible_770",nbinsX,xmin,xmax,nbinsY,ymin,ymax,nbinsZ,zmin,zmax);
hReconstructed_770   = TH3D("hReconstructed_770","hReconstructed_770",nbinsX,xmin,xmax,nbinsY,ymin,ymax,nbinsZ,zmin,zmax);
hReconstructible_9400 = TH3D("hReconstructible_9400","hReconstructible_9400",nbinsX,xmin,xmax,nbinsY,ymin,ymax,nbinsZ,zmin,zmax);
hReconstructed_9400   = TH3D("hReconstructed_9400","hReconstructed_9400",nbinsX,xmin,xmax,nbinsY,ymin,ymax,nbinsZ,zmin,zmax);

bufferFile = TFile("buffer.root","RECREATE")
reduced_treeMBias_mc      = treeMBias_mc.CopyTree(total_cut)
reduced_treeMBias_recoble = treeMBias_recoble.CopyTree(total_cut)
reduced_treeMBias_recoed  = treeMBias_recoed.CopyTree(total_cut)


htx_MBias_mc      = TH1F("htx_MBias_mc","htx_MBias_mc",100,-0.4,0.4)
htx_MBias_recoble = TH1F("htx_MBias_recoble","htx_MBias_recoble",100,-0.4,0.4)
htx_MBias_recoed  = TH1F("htx_MBias_recoed","htx_MBias_recoed",100,-0.4,0.4)


hty_MBias_mc      = TH1F("hty_MBias_mc","hty_MBias_mc",100,-0.3,0.3)
hty_MBias_recoble = TH1F("hty_MBias_recoble","hty_MBias_recoble",100,-0.3,0.3)
hty_MBias_recoed  = TH1F("hty_MBias_recoed","hty_MBias_recoed",100,-0.3,0.3)


honeOp_MBias_mc      = TH1F("honeOp_MBias_mc","honeOp_MBias_mc",100,0,0.001)
honeOp_MBias_recoble = TH1F("honeOp_MBias_recoble","honeOp_MBias_recoble",100,0,0.001)
honeOp_MBias_recoed  = TH1F("honeOp_MBias_recoed","honeOp_MBias_recoed",100,0,0.001)


h3D_MBias_mc      = TH3F("h3D_MBias_mc","h3D_MBias_mc",100,-0.4,0.4,100,-0.3,0.3,100,0,0.001)
h3D_MBias_recoble = TH3F("h3D_MBias_recoble","h3D_MBias_recoble",100,-0.4,0.4,100,-0.3,0.3,100,0,0.001)
h3D_MBias_recoed  = TH3F("h3D_MBias_recoed","h3D_MBias_recoed",100,-0.4,0.4,100,-0.3,0.3,100,0,0.001)



for event in reduced_treeMBias_mc:
    htx_MBias_mc.Fill(reduced_treeMBias_mc.tx_velo)
    hty_MBias_mc.Fill(reduced_treeMBias_mc.ty_velo)
    honeOp_MBias_mc.Fill(1./reduced_treeMBias_mc.p_velo)
    h3D_MBias_mc.Fill(reduced_treeMBias_mc.tx_velo,reduced_treeMBias_mc.ty_velo,1./reduced_treeMBias_mc.p_velo)
    
    
for event in reduced_treeMBias_recoble:
    htx_MBias_recoble.Fill(reduced_treeMBias_recoble.tx_velo)
    hty_MBias_recoble.Fill(reduced_treeMBias_recoble.ty_velo)
    honeOp_MBias_recoble.Fill(1./reduced_treeMBias_recoble.p_velo)
    h3D_MBias_recoble.Fill(reduced_treeMBias_recoble.tx_velo,reduced_treeMBias_recoble.ty_velo,1./reduced_treeMBias_recoble.p_velo)
    hReconstructible_770.Fill(reduced_treeMBias_recoble.tx_velo,reduced_treeMBias_recoble.ty_velo,1./reduced_treeMBias_recoble.p_velo)
    hReconstructible_9400.Fill(reduced_treeMBias_recoble.tx_calo,reduced_treeMBias_recoble.ty_calo,1./reduced_treeMBias_recoble.p_calo)
    
for event in reduced_treeMBias_recoed:
    htx_MBias_recoed.Fill(reduced_treeMBias_recoed.tx_velo)
    hty_MBias_recoed.Fill(reduced_treeMBias_recoed.ty_velo)
    honeOp_MBias_recoed.Fill(1./reduced_treeMBias_recoed.p_velo)
    h3D_MBias_recoed.Fill(reduced_treeMBias_recoed.tx_velo,reduced_treeMBias_recoed.ty_velo,1./reduced_treeMBias_recoed.p_velo)
    resX = reduced_treeMBias_recoed.tx_velo - reduced_treeMBias_recoed.true_tx_velo
    resY = reduced_treeMBias_recoed.ty_velo - reduced_treeMBias_recoed.true_ty_velo
    #resZ = 1./reduced_treeMBias_recoed.p_velo - 1./reduced_treeMBias_recoed.true_p_velo
    resZ = reduced_treeMBias_recoed.p_velo - reduced_treeMBias_recoed.true_p_velo
    tx = reduced_treeMBias_recoed.true_tx_velo
    ty = reduced_treeMBias_recoed.true_ty_velo
    #OneoP = 1./reduced_treeMBias_recoed.true_p_velo
    OneoP = reduced_treeMBias_recoed.true_p_velo
    arrayFill = np.array([tx,ty,OneoP,resX,resY,resZ])
    sparseRes_770.Fill(arrayFill) #Filling ResSparse at Velo
    resX = reduced_treeMBias_recoed.tx_calo - reduced_treeMBias_recoed.true_tx_calo
    resY = reduced_treeMBias_recoed.ty_calo - reduced_treeMBias_recoed.true_ty_calo
    #resZ = 1./reduced_treeMBias_recoed.p_calo - 1./reduced_treeMBias_recoed.true_p_calo
    resZ = reduced_treeMBias_recoed.p_calo - reduced_treeMBias_recoed.true_p_calo
    tx = reduced_treeMBias_recoed.true_tx_calo
    ty = reduced_treeMBias_recoed.true_ty_calo
    #OneoP = 1./reduced_treeMBias_recoed.true_p_calo
    OneoP = reduced_treeMBias_recoed.true_p_calo
    arrayFill = np.array([tx,ty,OneoP,resX,resY,resZ])
    sparseRes_9400.Fill(arrayFill) #Filling ResSparse at Calo
    hReconstructed_770.Fill(reduced_treeMBias_recoed.tx_velo,reduced_treeMBias_recoed.ty_velo,1./reduced_treeMBias_recoed.p_velo)
    hReconstructed_9400.Fill(reduced_treeMBias_recoed.tx_calo,reduced_treeMBias_recoed.ty_calo,1./reduced_treeMBias_recoed.p_calo)
    


h3D_MBias_Ratio_tx_recoble_mc = h3D_MBias_recoble.ProjectionX().Clone("h3D_MBias_Ratio_tx_recoble_mc")
h3D_MBias_Ratio_tx_recoed_recoble = h3D_MBias_recoed.ProjectionX().Clone("h3D_MBias_Ratio_tx_recoed_recoble")

h3D_MBias_Ratio_ty_recoble_mc = h3D_MBias_recoble.ProjectionY().Clone("h3D_MBias_Ratio_ty_recoble_mc")
h3D_MBias_Ratio_ty_recoed_recoble = h3D_MBias_recoed.ProjectionY().Clone("h3D_MBias_Ratio_ty_recoed_recoble")

h3D_MBias_Ratio_oneOp_recoble_mc = h3D_MBias_recoble.ProjectionZ().Clone("h3D_MBias_Ratio_oneOp_recoble_mc")
h3D_MBias_Ratio_oneOp_recoed_recoble = h3D_MBias_recoed.ProjectionZ().Clone("h3D_MBias_Ratio_oneOp_recoed_recoble")

#3dRatio
h3D_MBias_Ratio_recoble_mc      = h3D_MBias_recoble.Clone("h3D_MBias_Ratio_recoble_mc")
h3D_MBias_Ratio_recoed_recoble  = h3D_MBias_recoed.Clone("h3D_MBias_Ratio_recoed_recoble")

h3D_MBias_Ratio_recoble_mc.Divide(h3D_MBias_mc)
h3D_MBias_Ratio_recoed_recoble.Divide(h3D_MBias_recoble)
#End3DRatio


h3D_MBias_Ratio_tx_recoble_mc.Divide(h3D_MBias_mc.ProjectionX())
h3D_MBias_Ratio_tx_recoed_recoble.Divide(h3D_MBias_recoble.ProjectionX())

h3D_MBias_Ratio_ty_recoble_mc.Divide(h3D_MBias_mc.ProjectionY())
h3D_MBias_Ratio_ty_recoed_recoble.Divide(h3D_MBias_recoble.ProjectionY())

h3D_MBias_Ratio_oneOp_recoble_mc.Divide(h3D_MBias_mc.ProjectionZ())
h3D_MBias_Ratio_oneOp_recoed_recoble.Divide(h3D_MBias_recoble.ProjectionZ())
                                                 

output = TFile("ResSparse_2012.root","RECREATE")
output.cd()
sparseRes_770.Write()
sparseRes_9400.Write()
hReconstructible_770.Write()
hReconstructible_9400.Write()
hReconstructed_770.Write()
hReconstructed_9400.Write()
output.Close()
