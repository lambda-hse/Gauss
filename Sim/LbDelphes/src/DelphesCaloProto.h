#pragma once
// Include files
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include "Event/Particle.h"
#include "GaudiKernel/RndmGenerators.h"//for MC integration
//calo event
#include "Event/CaloHypo.h"
#include "Event/CaloCluster.h"
#include "Event/CaloDigit.h"
#include "Event/CaloPosition.h"
#include "Kernel/CaloCellID.h"
#include "CaloUtils/CaloAlgUtils.h"
#include "CaloInterfaces/ICaloHypoEstimator.h"
#include "Event/CaloCluster.h"
#include "Kernel/CaloCellCode.h"//for talking back and forth between conventions
//c++
#include <tuple>
#include <map>
#include <string>
#include <fstream>


class ExRootConfReader ;
class Delphes ;
// class DelphesFactory ;
// class TObjArray ;
//define some classes for more useful info than std::tuples
/** @class DelphesEcalTower DelphesCaloProto.h
 *  primitive holder for ECAL cell
 *
 *  @author Adam Davis
 *  @date   2018-6-1
 */
class DelphesEcalTower{
public:
  DelphesEcalTower(double cellxsize,double cellysize,std::string region,int row,int col, float energy){
    m_cellXsize = cellxsize;
    m_cellYsize = cellysize;

    m_region = region;
    m_row = row;
    m_col = col;
    m_energy = energy;

  }

  std::string Region(){return m_region;}
  int Row(){return m_row;}
  int Col(){return m_col;}
  float E(){return m_energy;}
  void SetE(float e){m_energy = e;}
  void SetTimeInfo(int t,int tmin,int tmax){
    m_time = t;
    m_timemin = tmin;
    m_timemax = tmax;
  }
  void SetCellXsize(double max_x_size){m_cellXsize = max_x_size;}
  void SetCellYsize(double max_y_size){m_cellYsize = max_y_size;}
  double GetCellXsize(){return m_cellXsize;}
  double GetCellYsize(){return m_cellYsize;}


private:
  std::string m_region{""};
  uint m_row{0};
  uint m_col{0};
  float m_energy{0};
  //size of cell for ease
  double m_cellXsize{0};
  double m_cellYsize{0};

  //for later
  int m_time{0};
  int m_timemin{0};
  int m_timemax{0};

};

/** @class DelphesSimEcalCluster DelphesCaloProto.h
 *  group of towers with necessary MC info
 *
 *  @author Adam Davis
 *  @date   2018-6-1
 */
class DelphesSimEcalCluster{
  //placeholder for delphes info
public:
  //constructor
  DelphesSimEcalCluster(std::string seedreg, int seedrow, int seedcol){
    m_seedreg = seedreg;
    m_seedrow = seedrow;
    m_seedcol = seedcol;


  };
  //define == operator
  // bool operator==(const DelphesSimEcalCluster a, const DelphesSimEcalCluster b){
  //   return (a.SeedRegion==b.SeedRegion
  //           && a.SeedRow()==b.SeedRow()
  //           && a.SeedCol()==b.SeedCol()
  //           && (
  std::string SeedRegion(){return m_seedreg;}
  uint SeedRow(){return m_seedrow;}
  uint SeedCol(){return m_seedcol;}
  void SetSeedEnergy(float energy){m_seedEnergy = energy;}
  float SeedEnergy(){return m_seedEnergy;}

  void AddTower(double cellxsize,double cellysize,std::string region, int row, int col, float energy){
    m_towers.push_back(DelphesEcalTower(cellxsize,cellysize,region,row,col,energy));}


  std::vector<DelphesEcalTower> Towers(){return m_towers;}
  void UpdateEnergy(std::string region, int row, int col, float newEnergy){
    //find tower matching region,row,col and update energy
    //good for spillover.
    //can do better than a loop here.
    for(auto t : m_towers){
      if(t.Region()==region && t.Row()==row &&t.Col()==col)t.SetE(newEnergy);
      break;
    }
  }
  float ClusterEnergy(){
    if(m_energy==-1){//recompute if uninitialized
      m_energy=0;
      for(auto t : m_towers){
        m_energy+=t.E();
      }
    }
    return m_energy;
  }
  void SetMCKey(int key){m_MCKey = key;}
  int MCKey(){return m_MCKey;}
  void SetMCEnergy(float e){m_mc_energy = e;}
  float MCEnergy(){return m_mc_energy;}


private:
  int m_MCKey{-2};
  std::string m_seedreg{""};
  int m_seedrow{0};
  int m_seedcol{0};
  float m_seedEnergy{-1};
  float m_energy{-1};
  float m_mc_energy{-1};

  std::vector<DelphesEcalTower> m_towers{};
};

typedef std::vector<DelphesSimEcalCluster> DelphesSimEcalClusters;


/** @class DelphesCaloProto DelphesCaloProto.h
 *  Separated from DelphesProto to be more modular
 *
 *  @author Adam Davis
 *  @date   2015-11-26
 *  @date   2018-1-29 add reader for calo geometry
 */
class DelphesCaloProto : public GaudiAlgorithm {
public:
  /// Standard constructor
  DelphesCaloProto( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization

  //  std::tuple<LHCb::ProtoParticle,int> NeutralProtoFromDelphes( const LHCb::MCParticle part );
// protected:
//   std::vector<double> AddInfo(double oneOverP, std::vector<std::vector<double>> lutMatrix);

private:

  Rndm::Numbers m_flatDist ;  ///< Flat random number generator
  Rndm::Numbers m_gaussDist ;//Gaussian random number generator

  Gaudi::Property<std::string> m_particles{this, "MCFastParticleLocation","/Event/MCFast/MCParticles",
      "Location of smeared MCParticles."}; ///< Location in TES of output smeared MCParticles.
  Gaudi::Property<std::string> m_neutralLocation{this,"MCFastNeutralProtoParticles",LHCb::ProtoParticleLocation::Neutrals,
      "Location to write neutral protoparticles"};// location in TES of output neutrals
  Gaudi::Property<std::string> m_photonsLocation{this,"MCFastPhotonsHypo",LHCb::CaloHypoLocation::Photons,
      "Location to write photon hypothesis"};
  Gaudi::Property<std::string> m_caloDigitsLocation{this, "MCFastCaloDigits",LHCb::CaloDigitLocation::Default,
      "Location to wrtie calo psedudigits"};

  Gaudi::Property<std::string> m_caloClusterLocation{this, "MCFastCaloClusterLocation",LHCb::CaloClusterLocation::Ecal,
      "Location to write calo clusters"};


  ///stuff for delphes calo reading
  Gaudi::Property<double> m_zEcal{this,"ZEcal",12650.5,"nominal z position of ecal to consider (front face)"};
  Gaudi::Property<int> m_CaloClusterLoopSize{this,"CaloClusterSize",3,
      "Number of cells for the calo clusterization to loop over, e.g 3=3x3"};
  Gaudi::Property<int> m_num_int_pts{this,"NumericalIntegrationPoints",4000,
      "number of points for calorimeter clusterization numerical integration"};

  Gaudi::Property<std::string> m_LHCbDelphesCard{this,"LHCbDelphesCardLocation","./delphes_card.tcl",
      "Location of the Delphes Card for specific LHCb Implementation"};
  Gaudi::Property<int> m_cellCenterX{this,"CaloCenterX",32,"central X cell of calo"};
  //central cell for CALO in X, assume 32 for LHCb Run I and II geo
  Gaudi::Property<int> m_cellCenterY{this,"CaloCenterY",32,"central Y cell of calo"};
  //central cell for CALO in Y, assume 32 for LHCb Run I and II geo
  Gaudi::Property<float> m_moliere_radius{this,"MoliereRadius",35.,"Moliere Radius (in mm)"};

  std::map<std::string,
           std::map<std::string,
                    std::map<std::string,double>
                    >
           > m_calo_spill_boundaries;  //x,y,cellsize
  std::vector<double> m_calo_x_borders;
  std::vector<double> m_calo_y_borders;
  std::vector<double> m_calo_x_bins;
  std::vector<double> m_calo_y_bins;//asssumes no crazy shapes, just a grid.
  ICaloHypoEstimator* m_estimator = nullptr;

  //intersection for moliere radius and calo cell
  std::vector<std::tuple<float,float> > PointsOfIntersection(float xhit, float yhit,
                                                             float xmin, float xmax,
                                                             float ymin, float ymax, float rM);

  float EnergyFraction(float x1, float x2, float y1, float y2, float num_RM, int num_integration_points =1000);

  std::vector<std::string>m_RegionNames;//pointer to names of regions.

  bool is_inside(float box_x1, float box_y1, float box_x2, float box_y2, float x, float y){
    debug()<<"now texting box with (x1,y1)--(x2,y2) ("<<box_x1<<","<<box_y1<<")--("<<box_x2<<","<<box_y2<<")"<<endmsg;
    debug()<<"point to test is ("<<x<<","<<y<<")"<<endmsg;

    if (x < box_x1 || x > box_x2) return 0;
    if (y < box_y1 || y > box_y2) return 0;
    return 1;
  }



};

