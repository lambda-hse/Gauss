// Include files 

 // from Gaudi
#include "GaudiKernel/ToolFactory.h"
#include "GaudiKernel/AlgFactory.h" 
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"
#include "GaudiKernel/DeclareFactoryEntries.h"
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/IRndmEngine.h"

#include "GaudiKernel/IDataManagerSvc.h"
// Event.
#include "Event/HepMCEvent.h"
#include "Event/MCParticle.h"
#include "Event/Particle.h"
#include "Event/RecVertex.h"

// from ROOT
#include "TDatabasePDG.h"
// from LHCb
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
//local
#include "DelphesProto.h"
//from ROOT
#include "TROOT.h"
#include "TMath.h"
#include "TString.h"
#include "TFormula.h"
#include "TObjArray.h"
#include "TDatabasePDG.h"
#include "TLorentzVector.h"
#include "TVector3.h"
#include "GaudiKernel/KeyedTraits.h"

//
#include "modules/Delphes.h"
#include "classes/DelphesClasses.h"
#include "classes/DelphesFactory.h"
#include "classes/DelphesHepMCReader.h"



// ============================================================================
// Relations 
// ============================================================================
//#include "Relations/Relation1D.h"
// ============================================================================


//-----------------------------------------------------------------------------
// Implementation file for class : DelphesProto
//
// 2015-01-19 : Patrick Robbe
// 2017-12-11 : Adam Davis, add neutral protoparticle maker
//-----------------------------------------------------------------------------


using namespace std;
// Declaration of the Algorithm Factory
DECLARE_COMPONENT( DelphesProto )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
DelphesProto::DelphesProto( const std::string& name,
                        ISvcLocator* pSvcLocator)
  : GaudiAlgorithm ( name , pSvcLocator ){}

//=============================================================================
// Initialization
//=============================================================================
StatusCode DelphesProto::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;
  //random number generator
  IRndmGenSvc * randSvc = svc< IRndmGenSvc >( "RndmGenSvc" , true ) ;
  sc = m_flatDist.initialize( randSvc , Rndm::Flat( 0. , 1. ) ) ;
  if(sc.isFailure()){
    error()<<"Cannot initialize flat random number generator"<<endmsg;
    return sc;    
  }
  sc = m_gaussDist.initialize( randSvc , Rndm::Gauss( 0. , 1. ) ) ;
  if(sc.isFailure()){
    error()<<"Cannot initialize flat random number generator"<<endmsg;
    return sc;    
  }
  
  existTrackLUT = false;
  
  if ( msgLevel(MSG::DEBUG) ) debug()<<"Loading look-up tables"<<endmsg;  
  if ( msgLevel(MSG::DEBUG) ) debug()<<"trying to load track file from "<<m_trackLUT.value().c_str()<<endmsg;
  
  std::ifstream lutTrackFile(m_trackLUT.value().c_str());  
  if(lutTrackFile.is_open()){
    if ( msgLevel(MSG::DEBUG) ) debug()<<"Loading Track Lookup Table"<<endmsg;   
    existTrackLUT = true;
    std::string line;  
    // while(lutTrackFile.good())
    while(getline(lutTrackFile,line)){
      std::vector<double> vecdummy;
      // getline(lutTrackFile,line);
      std::istringstream isline(line);
      std::string xmin;
      std::string xmax;
      isline>>xmin;
      isline>>xmax;
      // std::cout.precision(20);
      
      // cout<<"xmin: "<<xmin<<"\t xmax: "<<xmax<<endl;
      
      double minv = std::stod(xmin);
      double maxv = std::stod(xmax);
      // cout<<"xminv: "<<minv<<"\t xmaxv: "<<maxv<<endl;
      
      vecdummy.push_back(minv);
      vecdummy.push_back(maxv);
      std::string entry;     
      do{
        isline>>entry;
        vecdummy.push_back(std::stod(entry));
        
      }while(isline.good());
      lutTrackMatrix.push_back(vecdummy);      
    }
    lutTrackFile.close();  
  }
  if ( msgLevel(MSG::DEBUG) ) debug()<<"Loaded Track Look-Up table of size"<<lutTrackMatrix.size()<<endmsg;
  //check if actually loaded:
  if(lutTrackMatrix.size()==0){
    warning()<<"No file loaded! existTrackLUT = false!"<<endmsg;
    existTrackLUT=false;
  }
  
  if ( msgLevel(MSG::DEBUG) ) debug()<<"trying to load covariance file from "<<m_CovarianceLUT.value().c_str()<<endmsg;
  
  std::ifstream lutCovarianceFile(m_CovarianceLUT.value().c_str());
  if(lutCovarianceFile.is_open()){
    if ( msgLevel(MSG::DEBUG) ) debug()<<"Loading covariance look-up table"<<endmsg;    
    std::string line;  
    existCovarianceLUT = true;
    
    // while(lutCovarianceFile.good())
    while(getline(lutCovarianceFile,line)){
      std::vector<double> vecdummy;
      // getline(lutCovarianceFile,line);
      std::istringstream isline(line);
      std::string xmin;
      std::string xmax;
      isline>>xmin;
      isline>>xmax;
      // std::cout.precision(20);
      
      // cout<<"xmin: "<<xmin<<"\t xmax: "<<xmax<<endl;
      
      double minv = std::stod(xmin);
      double maxv = std::stod(xmax);
      //cout<<"xminv: "<<minv<<"\t xmaxv: "<<maxv<<endl;
      
      vecdummy.push_back(minv);
      vecdummy.push_back(maxv);
      std::string entry;     
      do{
        isline>>entry;
        vecdummy.push_back(std::stod(entry));
        
      }while(isline.good());
      lutCovarianceMatrix.push_back(vecdummy);      
    }
    lutCovarianceFile.close();  
  }
  if ( msgLevel(MSG::DEBUG) ) debug()<<"Loaded covariance lookup table of size "<<lutCovarianceMatrix.size()<<endmsg;
  //add failsafe
  if(lutCovarianceMatrix.size()==0){
    warning()<<"covariance lookup table not found! existCovarianceLUT = false!"<<endmsg;
    existCovarianceLUT=false;   
  }
  
  //m_estimator = tool<ICaloHypoEstimator>("CaloHypoEstimator","CaloHypoEstimator",this);
  
  
  //if ( msgLevel(MSG::DEBUG) ) debug()<<"cell size for region 1 = "<<size_reg1x <<" x "<<size_reg1y<<endmsg;
  release( randSvc ) ;
  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode DelphesProto::execute() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;
  
  
  //Get TES container
  LHCb::ProtoParticles* protoContainer = getOrCreate<LHCb::ProtoParticles,LHCb::ProtoParticles>( m_protoParticles );

  LHCb::RichPIDs* richPIDContainer = getOrCreate<LHCb::RichPIDs,LHCb::RichPIDs>( m_richPIDs );
  LHCb::MuonPIDs* muonPIDContainer = getOrCreate<LHCb::MuonPIDs,LHCb::MuonPIDs>( m_muonPIDs );
  
  LHCb::Tracks* trackContainer = getOrCreate<LHCb::Tracks,LHCb::Tracks>( m_tracks );

  LHCb::MCParticles *m_particleContainer = get<LHCb::MCParticles>(m_particles);
  
  
  //loop on MC particles.
  
  LHCb::MCParticles::const_iterator i;
  for( i=m_particleContainer->begin(); i != m_particleContainer->end(); i++ ){
    
    LHCb::MCParticle *part = (*i);
        
    const LHCb::MCVertex *Vtx = part->endVertices()[0];
    const LHCb::MCVertex *OrigVtx = (LHCb::MCVertex*)part->primaryVertex();
    const LHCb::ParticleID &partID = part->particleID();
    if ( msgLevel(MSG::DEBUG) )debug()<<"processing PID "<<partID.pid()<<endmsg;
    //Charged particles
    
    if(!(partID.pid()==22 || partID.pid()==111 || partID.pid()==2112)){//no photons, pi0s or neutrons
      
      Int_t charge = partID.threeCharge()/3;
      Double_t p = part->momentum().P();
      double qOverP = charge / p;
      
      Double_t px = part->momentum().Px();
      Double_t py = part->momentum().Py();
      Double_t pz = part->momentum().Pz();
      Double_t tx = px/pz;
      Double_t ty = py/pz;

      const Gaudi::XYZPoint &position = Vtx->position();
      const Gaudi::XYZPoint &orig_position = OrigVtx->position();
  
      LHCb::ProtoParticle *proto = new LHCb::ProtoParticle;
      LHCb::Track *track = new LHCb::Track(part->key());
      if(existTrackLUT)
      {        
        std::vector<double> addedInfo = AddInfo(1./p,lutTrackMatrix);        
        if (addedInfo.size() != 0) 
        {
          double chi2 = m_gaussDist()*addedInfo[1]+addedInfo[0];        
          int nDoF = m_gaussDist()*addedInfo[3]+addedInfo[2];
          double ghostProb = m_gaussDist()*addedInfo[5]+addedInfo[4];
          double likelihood = m_gaussDist()*addedInfo[7]+addedInfo[6];

          track->setFitStatus(LHCb::Track::Fitted);
          track->setHistory(LHCb::Track::TrackIdealPR);
          track->setChi2PerDoF(chi2);
          track->setNDoF(nDoF);
          track->setGhostProbability(ghostProb);
          track->setLikelihood(likelihood);
        }
      }
      LHCb::State *trackState = new LHCb::State();
      trackState->setState(position.X(), position.Y(), position.Z(), tx, ty, qOverP);   
      trackState->setLocation(LHCb::State::EndRich2);

      if(existCovarianceLUT)
      {
        Gaudi::TrackSymMatrix cov;
        std::vector<double> addedCovariance = AddInfo(1./p,lutCovarianceMatrix);
        if (addedCovariance.size() != 0)
        {
          int icov = 0;
          
          for(int i = 0; i<5; i++)
            for(int j=0; j<5; j++)
            {
              cov(i,j) = m_gaussDist()*addedCovariance[icov+1] + addedCovariance[icov];
              icov+=2;            
            }
          trackState->setCovariance( cov );
        }
      }
      track->addToStates(*trackState);
      track->setType(LHCb::Track::Long);
      

      // std::unique_ptr<LHCb::RichPID> richPID;
      // std::unique_ptr<LHCb::MuonPID> muonPID;

      std::unique_ptr<LHCb::RichPID>richPID = std::make_unique<LHCb::RichPID>();
      std::unique_ptr<LHCb::MuonPID>muonPID = std::make_unique<LHCb::MuonPID>();
      

      if ( msgLevel(MSG::DEBUG) )  debug()<<"Initialized richPID and muonPID "<<endmsg;
      
      const std::vector< float > llValue{-1000.0,0.0,0.0,0.0,0.0,0.0,0.0};

      muonPID->setIDTrack(track);
      richPID->setTrack(track);
      if ( msgLevel(MSG::DEBUG) ) debug()<<"Setting RichPID default"<<endmsg;
      
      switch(abs(partID.pid()))
      {
      case(11):
        richPID->setBestParticleID(Rich::Electron);
        break;
      case(13):
        richPID->setBestParticleID(Rich::Muon);
        break;
      case(211):
        richPID->setBestParticleID(Rich::Pion);
        break;
      case(321):
        richPID->setBestParticleID(Rich::Kaon);
        break;
      case(2212):
        richPID->setBestParticleID(Rich::Proton);
        break;
      default:
        richPID->setBestParticleID(Rich::Unknown);
      }
      proto->addInfo(LHCb::ProtoParticle::NoPID , 1);
      proto->addInfo(LHCb::ProtoParticle::TrackChi2PerDof,1);
      
      proto->setTrack(track);
      // proto->setRichPID ( richPID.get() ) ; 
      if ( msgLevel(MSG::DEBUG) ) debug()<<"Adding to the containers"<<endmsg;
      
      // if(abs(partID.pid())!= 211) continue;
      richPIDContainer->insert(richPID.release(),part->key());    
      muonPIDContainer->insert(muonPID.release(),part->key());    
      trackContainer->insert(track,part->key());      
      protoContainer->insert(proto,part->key());
      if ( msgLevel(MSG::DEBUG) ) debug()<<"Added to the relative containers"<<endmsg;
 
    }
  
  }//mc loop
  
  return StatusCode::SUCCESS;
}


  
//=============================================================================
//  Additional Infos
//=============================================================================
std::vector<double> DelphesProto::AddInfo(double oneOverP, std::vector<std::vector<double>> lutMatrix) {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Adding Info" << endmsg;
  
  std::vector<double> addedInfo;
  
  for(uint rows = 0; rows<lutMatrix.size(); rows++)
  {
    if((oneOverP >= lutMatrix[rows][0]) && (oneOverP <= lutMatrix[rows][1]))
    {
      for(uint iInfo=2; iInfo<lutMatrix[rows].size(); iInfo++)
        addedInfo.push_back(lutMatrix[rows][iInfo]);      
      break;      
    }    
  }
  return addedInfo;  
}
