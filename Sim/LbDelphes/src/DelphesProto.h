#pragma once
// Include files 
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include "Event/Particle.h"
#include "GaudiKernel/RndmGenerators.h"//for MC integration
//calo event
#include "Event/CaloHypo.h"
#include "Event/CaloCluster.h"
#include "Event/CaloDigit.h"
#include "Event/CaloPosition.h"
#include "Kernel/CaloCellID.h"
#include "CaloUtils/CaloAlgUtils.h"
#include "CaloInterfaces/ICaloHypoEstimator.h"
#include "Event/CaloCluster.h"
#include "Kernel/CaloCellCode.h"//for talking back and forth between conventions
//c++
#include <tuple>
#include <map>
#include <string>
#include <fstream>


class ExRootConfReader ;
class Delphes ;

  
/** @class DelphesProto DelphesProto.h
 *  
 *
 *  @author Benedetto Gianluca Siddi
 *  @date   2015-11-26
 *  @date   2018-1-29 add reader for calo geometry
 */
class DelphesProto : public GaudiAlgorithm {
public: 
  /// Standard constructor
  DelphesProto( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  
private:
  
  Rndm::Numbers m_flatDist ;  ///< Flat random number generator
  Rndm::Numbers m_gaussDist ;//Gaussian random number generator
  
  Gaudi::Property<std::string> m_particles{ this, "MCFastParticleLocation","/Event/MCFast/MCParticles", "Location to read smeared MCParticles." };///< Location in TES of output smeared MCParticles.

  Gaudi::Property<std::string> m_vertices{ this, "MCFastVerticesLocation","/Event/MCFast/MCVertices", "Location to read smeared MCVertices." };///< Location in TES of output smeared MCVertices

  Gaudi::Property<std::string> m_protoParticles{ this, "MCFastProtoParticles",LHCb::ProtoParticleLocation::Charged, "Location to write charged ProtoParticles." };//< Location in TES of output ProtoParticles

  Gaudi::Property<std::string> m_tracks{ this,"MCFastTrack",LHCb::TrackLocation::Default,"Location to write Tracks."};
  Gaudi::Property<std::string> m_richPIDs{ this,"MCFastRichPID",LHCb::RichPIDLocation::Default,"Location to write RichPIDs."};
  Gaudi::Property<std::string> m_muonPIDs{ this,"MCFastMuonPID",LHCb::MuonPIDLocation::Default,"Location to write MuonPIDs."};
  Gaudi::Property<std::string> m_trackLUT{ this,"TrackLUT","$LBDELPHESROOT/LookUpTables/lutTrack.dat","Lookup Table for track quantities"};
  Gaudi::Property<std::string> m_CovarianceLUT{ this,"TrackCovarianceLUT","$LBDELPHESROOT/LookUpTables/lutCovarianceProf.dat","Lookup Table for track covariance matrix"};
  

  std::vector<std::vector<double>> lutTrackMatrix; //Matrix table for tracks quantities
  bool existTrackLUT = false;//check if lookup table exists
  std::vector<std::vector<double>> lutCovarianceMatrix;//Covariance Matrix table 
  bool existCovarianceLUT = false;//check if covariance matrix lut exists

  std::vector<double> AddInfo(double oneOverP, std::vector<std::vector<double>> lutMatrix);




};

