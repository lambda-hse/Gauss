#pragma once
// Include files 
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include "Event/Particle.h"
#include "GaudiKernel/RndmGenerators.h"//for MC integration
#include "GaudiKernel/Property.h"//for MC integration

#include "Event/RecSummary.h" 

#include "GaudiTensorFlow/Predictor.h" 
namespace gtf=GaudiTensorFlow;

#include <memory>

#include <unordered_map>
  
/** @class DelphesParticleId DelphesParticleId.h
 *
 *  @author Lucio Anderlini
 *  @date   2018-12-10  File created
 */
class DelphesParticleId : public GaudiAlgorithm 
{
  public: 
    /// Standard constructor
    DelphesParticleId( const std::string& name, ISvcLocator* pSvcLocator );

    virtual ~DelphesParticleId( ); ///< Destructor

    virtual StatusCode initialize();    ///< Algorithm initialization
    virtual StatusCode execute   ();    ///< Algorithm execution
    virtual StatusCode finalize  ();    ///< Algorithm finalization

    typedef double t_RichFloat;
    typedef float  t_MuonFloat; 

  private:

////////////////////////////////////////////////////////////////////////////////
// Input configuration                                                        //
////////////////////////////////////////////////////////////////////////////////
    Gaudi::Property<std::string> m_input_particles {this, 
      "InputLocation", "/Event/MCFast/MCParticles",
      "Input TES location of particles"};

    Gaudi::Property<std::string> m_recsummary_location {this, 
      "RecSummaryLocation", LHCb::RecSummaryLocation::Default,
      "Input TES location of RecSummary"};

////////////////////////////////////////////////////////////////////////////////
// Rich PID configuration                                                     //
////////////////////////////////////////////////////////////////////////////////
    Gaudi::Property<std::string> m_rich_pid {this, 
      "RichPIDsLocation", LHCb::RichPIDLocation::Default,
      "TES locations of the RichPID objects to update"};

    Gaudi::Property< std::map<std::string,std::string> > m_rich_gan{this,
      "RichGan", std::map<std::string,std::string>(),
      "Export directory for RICH Gan"}; 

    Gaudi::Property<std::string> m_rich_input{this,
      "RichGanInput", "x",
      "Name of the tensor used as an input for Rich Gan"}; 

    Gaudi::Property<std::string> m_rich_output{this,
      "RichGanOutput", "QuantileTransformerTF_3/stack",
      "Name of the tensor from which to collect the output for Muon Gan"}; 

    std::unordered_map < int , std::unique_ptr<gtf::Predictor> > m_richNN; 
  
////////////////////////////////////////////////////////////////////////////////
// Muon PID configuration                                                     //
////////////////////////////////////////////////////////////////////////////////
    Gaudi::Property<std::string> m_muon_pid {this, 
      "MuonPIDsLocation", LHCb::MuonPIDLocation::Default,
      "TES locations of the MuonPID objects to update"};

    Gaudi::Property< std::map<std::string,std::string> > m_isMuon_mlp{this,
      "isMuonMlp", std::map<std::string,std::string>(),
      "Export directory for isMuon Gan"}; 

    Gaudi::Property < std::string > m_isMuon_input{this,
      "isMuonMlpInput", "inputds",
      "Name of the tensor used as an input for Muon Gan"}; 

    Gaudi::Property<std::string> m_isMuon_output{this,
      "isMuonMlpOutput", "strided_slice",
      "Name of the tensor from which to collect the output for Muon Gan"}; 


    Gaudi::Property< std::map<std::string,std::string> > m_muon_gan{this,
      "MuonLLGan", std::map<std::string,std::string>(),
      "Export directory for Muon Gan"}; 

    Gaudi::Property < std::string > m_muon_input{this,
      "MuonLLGanInput", "X_gen",
      "Name of the tensor used as an input for Muon Gan"}; 

    Gaudi::Property<std::string> m_muon_output{this,
      "MuonLLGanOutput", "Y_gen_1",
      "Name of the tensor from which to collect the output for Muon Gan"}; 

    std::unordered_map < int, std::pair<
      std::unique_ptr<gtf::Predictor>, std::unique_ptr<gtf::Predictor> > > m_muonNN; 


  private: // methods 
    std::vector <int> getKeysOfParticles(int mcId);
    StatusCode evalRich ( std::vector<int> keys, gtf::Predictor& gan );
    StatusCode evalMuon ( std::vector<int> keys, 
                          gtf::Predictor*  mlp,
                          gtf::Predictor*  gan 
          ); 
};


