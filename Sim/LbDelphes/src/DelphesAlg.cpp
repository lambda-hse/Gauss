// Include files 
#include <fstream>
#include <string>
#include <experimental/filesystem>

// from Gaudi
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"

#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/IRndmEngine.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/IDataManagerSvc.h"



// Event.
#include "Event/HepMCEvent.h"
#include "Event/MCParticle.h"
#include "Event/Particle.h"

// HepMC.
#include "GenEvent/HepMCUtils.h"

// from DELPHES
#include "modules/Delphes.h"
#include "classes/DelphesClasses.h"
#include "classes/DelphesFactory.h"
#include "classes/DelphesHepMCReader.h"
#include "ExRootAnalysis/ExRootConfReader.h"
// from ROOT
#include "TDatabasePDG.h"

// local
#include "DelphesAlg.h"

// from LHCb
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

//from ROOT
#include "TROOT.h"
#include "TMath.h"
#include "TString.h"
#include "TFormula.h"
#include "TRandom3.h"
#include "TObjArray.h"
#include "TLorentzVector.h"
#include "TVector3.h"

#include "GaudiKernel/KeyedTraits.h"

//-----------------------------------------------------------------------------
// Implementation file for class : DelphesAlg
//
// 2015-01-19 : Patrick Robbe
// 2018 - : Adam Davis, update for use with PGPrimaryVertex
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( DelphesAlg )
//=============================================================================
// Standard constructor, initializes variables

DelphesAlg::DelphesAlg( const std::string& name,ISvcLocator* pSvcLocator)
: GaudiAlgorithm ( name , pSvcLocator ){}

//=============================================================================
// Initialization
//=============================================================================
StatusCode DelphesAlg::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;

  // gROOT->SetBatch();
  key_gen = 0;
  
  m_confReader = std::make_unique<ExRootConfReader>();
  m_confReader->ReadFile(m_LHCbDelphesCard.value().c_str());
  
  m_modularDelphes = std::make_unique<Delphes>("Delphes");  
  m_modularDelphes->SetConfReader(m_confReader.get());
  
  m_Delphes = m_modularDelphes->GetFactory();
  //we do not own these objects, therefore we keep them as raw pointers.
  m_allParticleOutputArray = m_modularDelphes->ExportArray("allParticles");
  m_stableParticleOutputArray = m_modularDelphes->ExportArray("stableParticles");
  m_partonOutputArray = m_modularDelphes->ExportArray("partons");
  
  m_modularDelphes->InitTask();

  std::cout << "===============Initialize Geometry===============" << std::endl;

  //// fGeometryData = GetString("GeometryData", "data.txt");
  // std::ifstream geometry_file("$LBDELPHESROOT/options/geometry_data.txt");
  std::ifstream geometry_file("geometry_data.txt");
  std::cout << "Current path is " << std::experimental::filesystem::current_path() << '\n';
  std::string line;
  if (geometry_file.is_open()) {
      std::cout << "============== geometry_data is open ===============" << '\n';
      for (std::size_t y = num_y_modules; y >= 1; --y) { // we define array in order to have 0,0 at left bottom corner
          for (std::size_t x = 0; x < num_x_modules; ++x) {
              geometry_file >> ecal_modules[y - 1][x];
          }
      }
      //while (getline(geometry_file, line)) {
      //    std::cout << line << '\n';
      //}

      geometry_file.close();

      for (std::size_t y = num_y_modules; y >= 1; --y) {
          for (std::size_t x = 0; x < num_x_modules; ++x) {
              std::cout << ecal_modules[y-1][x];
          }
          std::cout << '\n';
      }
  }
  else {
      std::cout << "Cannot open data file.\n";
      // return 0;
  }

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode DelphesAlg::execute() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;

  m_modularDelphes->Clear();

  // convert hep mc -> Delphes
  LHCb::HepMCEvents* generationEvents = 
    get<LHCb::HepMCEvents>(m_generationLocation);

  //Get TES container
  LHCb::MCParticles *m_genParticleContainer = getOrCreate<LHCb::MCParticles,LHCb::MCParticles>(m_generatedParticles);
  LHCb::MCVertices  *m_genVerticesContainer = getOrCreate<LHCb::MCVertices,LHCb::MCVertices>(m_generatedVertices);

  LHCb::MCParticles *m_particleContainer = getOrCreate<LHCb::MCParticles,LHCb::MCParticles>(m_particles);
  LHCb::MCVertices  *m_verticesContainer = getOrCreate<LHCb::MCVertices,LHCb::MCVertices>(m_vertices);
   
  LHCb::MCParticles *m_simpleParticleContainer = getOrCreate<LHCb::MCParticles,LHCb::MCParticles>(m_simpleParticles);
  
  Candidate *candidate{nullptr};
  
  TDatabasePDG *pdg{nullptr};
  TParticlePDG *pdgParticle{nullptr};
  Int_t pdgCode;

  Int_t pid( 0 ) , status( 0 );
  //std::vector<HepMC::GenVertex> generated_pvs;
  std::vector<int>generated_PV_barcodes;  
  pdg = TDatabasePDG::Instance();
  //loop over all HepMC Events
  for(LHCb::HepMCEvents::const_iterator genEvent = generationEvents->begin(); 
      generationEvents->end() != genEvent; ++genEvent) {
    if ( msgLevel(MSG::DEBUG) ){
      
      debug()<<"----------From HepMC Event----------------"<<endmsg;
      debug()
        <<"PID"<<std::setw(12)
        <<"Status"<<std::setw(12)
        <<"Px"<<std::setw(12)
        <<"Py"<<std::setw(12)
        <<"Pz"<<std::setw(12)
        <<"Mass"<<std::setw(12)
        <<"Energy"<<std::setw(12)
        <<"x"<<std::setw(12)
        <<"y"<<std::setw(12)
        <<"z"<<std::setw(12)
        <<"M1"<<std::setw(12)
        <<"MCKey"<<std::setw(12)
        <<"Eta"
        <<endmsg ;
    }
    
    //Retrieve the event.
    
    
    HepMC::GenEvent* ev = (*genEvent)->pGenEvt();
    //put pointer to PV
    //stolen from GenerationToSimulation
    HepMC::GenVertex* genPV = primaryVertex(ev);    
    if(nullptr==genPV){//need a check, otherwise we have a seg fault
      Warning("Failed to find a PV in the event!!!").ignore();
      return StatusCode::FAILURE;
    }
    //if ( msgLevel(MSG::DEBUG) ) debug()<<"got generated PV"<<genPV->barcode()<<endmsg;
    generated_PV_barcodes.push_back(genPV->barcode());
    
    //loop over particles in hep mc event
    for (HepMC::GenEvent::particle_const_iterator itP = ev->particles_begin();
         itP != ev->particles_end(); ++itP) {
      //make delphes candidate
      //if we have a beam particle, don't consider it for Delphes propagation:
      if((*itP)==ev->beam_particles().first || (*itP)==ev->beam_particles().second)
      {
        if(msgLevel(MSG::DEBUG) )debug()<<"found beam particle!"<<endmsg;
        continue;
        
      }
      
      (*itP)->set_generated_mass(key_gen);

      candidate = m_Delphes->NewCandidate();
      
      candidate->PID = (*itP) -> pdg_id() ;
      pid = candidate -> PID ;
      // if ( msgLevel(MSG::DEBUG) ) debug()<<"now on candidate with PID "<<pid<<endmsg;
      // if ( msgLevel(MSG::DEBUG) ) debug()<<"from hepMC particle, mass = "<<(*itP)->generated_mass () 
      //                                    <<", and from momentum "<<(*itP)->momentum().m()<<endmsg;
      
      pdgCode = TMath::Abs(candidate->PID);
      
      candidate->Status = (*itP) -> status() ;
      status = candidate -> Status ;
      
      pdgParticle = pdg->GetParticle(pid);
      candidate->Charge = pdgParticle ? Int_t(pdgParticle->Charge()/3.0) : -999;
      candidate->Mass = (*itP) -> generated_mass() ;
      
      candidate->Momentum.SetPxPyPzE((*itP) -> momentum().x() , 
                                     (*itP) -> momentum().y() , 
                                     (*itP) -> momentum().z(), 
                                     (*itP) -> momentum().t() ) ;
      //if ( msgLevel(MSG::DEBUG) ) debug()<<"does the particle have a production vertex? "<<(*itP)->production_vertex() <<endmsg;

      int mother_id =-1;
      int mother_pid=-1;
      
      if ( 0 != (*itP)->production_vertex() ) 
      {
        candidate->Position.SetXYZT( (*itP) -> production_vertex()->position().x() , 
                                     (*itP) -> production_vertex()->position().y() , 
                                     (*itP) -> production_vertex()->position().z() , 
                                     (*itP) -> production_vertex()->position().t() ) ;
        
        for (HepMC::GenVertex::particle_iterator mother = (*itP)->production_vertex()->particles_begin(HepMC::parents);
                      mother!=(*itP)->production_vertex()->particles_end(HepMC::parents);
                      ++mother ) {
                      mother_id = static_cast<int>((*mother)->generated_mass());
                      mother_pid = (*mother)->pdg_id();
                      // std::cout << "mother_id: " << mother_id
                      //            << " mother_pid: " << mother_pid << '\n';
        }
      }
      const TLorentzVector &mc_momentum = candidate->Momentum;
      
      candidate->M2 = genPV->barcode();//assign M2 to have the barcode of the PV.    
      candidate->SetUniqueID(key_gen);
      //set the info of the candidate particle      
      candidate->M1 = key_gen;
      auto MCpart = std::make_unique<LHCb::MCParticle>();
      LHCb::ParticleID MCpartID;
      MCpartID.setPid(candidate->PID);
      MCpart->setParticleID(MCpartID);
      Gaudi::LorentzVector gaudi_momentum;
      Double_t modMCP2 = TMath::Power(mc_momentum.Px(),2) + TMath::Power(mc_momentum.Py(),2) + TMath::Power(mc_momentum.Pz(),2); 
      Double_t enMC = TMath::Sqrt(modMCP2 + TMath::Power(candidate->Mass,2));
      gaudi_momentum.SetPxPyPzE(mc_momentum.Px(),mc_momentum.Py(),mc_momentum.Pz(),enMC);
      const TLorentzVector &position= candidate->Position;
      

      if ( msgLevel(MSG::DEBUG) ){
        
        char num1[50],num2[50],num3[50],num4[50];
        sprintf(num1,"%.8f",candidate->Momentum.Px());
        sprintf(num2,"%.8f",candidate->Momentum.Py());
        sprintf(num3,"%.8f",candidate->Momentum.Pz());
        sprintf(num4,"%.8f",candidate->Momentum.E());
        debug()<<"for particle pid"<<candidate->PID<<" and m "<<candidate->Mass
               <<", (px,py,pz,E) = ("<<num1<<","<<num2<<","<<num3<<","<<num4<<")"<<endmsg;
      }
      
      MCpart->setMomentum(gaudi_momentum);
      

      Gaudi::XYZPoint point(position.X(),
                            position.Y(),
                            position.Z());

      //set MC vertex position.
      auto origVtx = std::make_unique<LHCb::MCVertex>();
      
      origVtx->setPosition(point);
      origVtx->setTime(position.T());

      LHCb::MCParticles::const_iterator savedPart;
      for (savedPart = m_genParticleContainer->begin(); savedPart != m_genParticleContainer->end(); savedPart++) {
          // std::cout << "(*savedPart)->key()=" << (*savedPart)->key() << ' ' << mother_id << "=mother_id\n";
          if ((*savedPart)->key() == mother_id) {
              origVtx->setMother(*savedPart);
          }
      }

      //if ( msgLevel(MSG::DEBUG) ) debug()<<"got production vertex"<<(*itP) -> production_vertex()->barcode()<<endmsg;

      //set the type of MC vertex. needed for PGPrimaryVertex to work
      if((*itP) -> production_vertex()->barcode()==candidate->M2){
        
        //if ( msgLevel(MSG::DEBUG) ) debug()<<"found PV at ("<<point.X()<<","<<point.Y()<<","<<point.Z()<<")"<<endmsg;        
        origVtx->setType(LHCb::MCVertex::ppCollision);
        //if ( msgLevel(MSG::DEBUG) ) debug()<<"testing PV->isPrimary()"<<origVtx->isPrimary()<<endmsg;
      }
      else{origVtx->setType(LHCb::MCVertex::GenericInteraction);}
      MCpart->setOriginVertex(origVtx.get());
      Double_t eta  = MCpart->pseudoRapidity();
      if ( msgLevel(MSG::DEBUG) ){
        
        debug()
          <<candidate->PID<<std::setw(12)
          <<candidate->Status<<std::setw(12)
          <<candidate->Momentum.Px()<<std::setw(12)
          <<candidate->Momentum.Py()<<std::setw(12)
          <<candidate->Momentum.Pz()<<std::setw(12)
          <<candidate->Mass<<std::setw(12)
          <<enMC<<std::setw(12)
          <<candidate->Position.X()<<std::setw(12)
          <<candidate->Position.Y()<<std::setw(12)
          <<candidate->Position.Z()<<std::setw(12)
          <<candidate->M1<<std::setw(12)
          <<MCpart->key()<<std::setw(12)
          <<eta
          <<endmsg ;
      }
      m_genVerticesContainer->insert(origVtx.release());
      m_genParticleContainer->insert(MCpart.release(),key_gen);
      key_gen++;

      
      m_allParticleOutputArray->Add(candidate);
   
      if(!pdgParticle) continue ;
      
      if((status == 1) || (status==999))
      {
        if ( msgLevel(MSG::DEBUG) ) debug()<<"adding candidate PID "<<candidate->PID<<" to stable particles"<<endmsg;
        
        m_stableParticleOutputArray->Add(candidate);
      }
      else if(pdgCode <= 5 || pdgCode == 21 || pdgCode == 15){
        m_partonOutputArray->Add(candidate);
      }
    }
  }//end loop on generated particles and the HepMC event
  
  debug()<<"processing task in delphes"<<endmsg;  
  m_modularDelphes->ProcessTask();//make delphes talk
  ///get tracks from delphes
  const TObjArray *arrayTracks = m_modularDelphes->ImportArray(m_track_location.value().c_str());

  TIter iteratorTracks(arrayTracks);
  if ( msgLevel(MSG::DEBUG) ){  
    debug()<<"+++++++++++++++++++++++++++++++++++++"<<endmsg;
    debug()<<"dumping final contents of TrackMerger"<<endmsg;    
    iteratorTracks.Reset();
    debug()<<"PID"<<std::setw(12)
           <<"Status"<<std::setw(12)
           <<"E"<<std::setw(12)
           <<"Px"<<std::setw(12)
           <<"Py"<<std::setw(12)
           <<"Pz"<<std::setw(12)
           <<"x"<<std::setw(12)
           <<"y"<<std::setw(12)
           <<"z"<<std::setw(12)
           <<"M1"<<std::setw(12)
           <<"M2"<<std::setw(12)
           <<endmsg;      
    Candidate* thing;
    while(thing  = static_cast<Candidate*>(iteratorTracks.Next())){
      debug()
        <<thing->PID<<std::setw(12)
        <<thing->Status<<std::setw(12)
        <<thing->Momentum.E()<<std::setw(12)
        <<thing->Momentum.Px()<<std::setw(12)
        <<thing->Momentum.Py()<<std::setw(12)
        <<thing->Momentum.Pz()<<std::setw(12)
        <<thing->Position.X()<<std::setw(12)
        <<thing->Position.Y()<<std::setw(12)
        <<thing->Position.Z()<<std::setw(12)
        <<thing->M1
        <<thing->M2
        <<endmsg ;
    }
    debug()<<"+++++++++++++++++++++++++++++++++++++"<<endmsg;

  }

  iteratorTracks.Reset();
  if ( msgLevel(MSG::DEBUG) ){
    debug()<<"----------From TrackMerger----------------"<<endmsg;
    debug()
      <<"PID"<<std::setw(12)
      <<"Status"<<std::setw(12)
      <<"Px"<<std::setw(12)
      <<"Py"<<std::setw(12)
      <<"Pz"<<std::setw(12)
      <<"Mass"<<std::setw(12)
      <<"Energy"<<std::setw(12)
      <<"x"<<std::setw(12)
      <<"y"<<std::setw(12)
      <<"z"<<std::setw(12)
      <<"t"<<std::setw(12)
      <<"x_0"<<std::setw(12)
      <<"y_0"<<std::setw(12)
      <<"z_0"<<std::setw(12)
      <<"t_0"<<std::setw(12)
      <<"M1"<<std::setw(12)
      <<"Eta"<<std::setw(12)
      <<"M2"
      <<endmsg ;
  }

  //loop over containers
  for (int stage = 0; stage < 2; ++stage) {

    iteratorTracks.Reset();
    Candidate *particle;
    Candidate *mother;
    Candidate *first_particle;
    int x_module, y_module;
    double module_pitch = 121.2;
    double cell_pitch[] = { 0.0, 121.2, 60.6, 40.4 };
    double x, y;
    //double inner_cell_pitch = 40.4;
    //double middle_cell_pitch = 60.6;
    //double outer_cell_pitch = 121.2; // module pitch

    double ECAL_BorderX = 3878.4, ECAL_BorderY = 3151.2;
    // set BorderX[list 323.2 969.6 1939.2 3878.4]
    // set BorderY[list 323.2 727.2 1212.0 3151.2]

    //loop over tracks
    while((mother = static_cast<Candidate*>(iteratorTracks.Next())))
    {
      if ( msgLevel(MSG::DEBUG) ) debug()<<"status = of this mother: "<<mother->Status<<endmsg;
      bool isAlreadyContained = false;        
      //mother here is the mother of the object, not the mother in the decay.
      if ( msgLevel(MSG::DEBUG) ) debug()<<"checking for time hits:"<<mother->NTimeHits<<endmsg;
      if(!(mother->Status==1 || mother->Status==999)){
        if ( msgLevel(MSG::DEBUG) ) {
          debug()<<"Now considering Mother PID "<<mother->PID<<", Key"<<mother->M1<<", UniqueID "<<mother->GetUniqueID()<<endmsg;
        }
        
        //propbably this key is already in the container, and it's a duplicate charged track from the calo
        //look explicitly for it
        int sizeOfCandidates = mother->GetCandidates()->GetEntries();
        if ( msgLevel(MSG::DEBUG) ) debug()<<"there are "<<sizeOfCandidates<<" candidates to consider"<<endmsg;      
        
        Candidate* candUp;
        TIter itMother(mother->GetCandidates());      
        while(candUp=static_cast<Candidate*>(itMother.Next())){
          long PartKey = ((Candidate*)candUp)->M1;
          int PartPID = ((Candidate*)candUp)->PID;
          if ( msgLevel(MSG::DEBUG) ){
            debug()<<"looping on candidates in mother, production vertex = "<<candUp->M2<<", with PID "<<PartPID<<endmsg;
          
            debug()<<"\t Key "<<PartKey<<", PID "<<PartPID<<"uniqueID "<< candUp->GetUniqueID()
                  <<", contained: "<<m_particleContainer->containedObject(PartKey)<<endmsg;
            //debug()<<"endvertex (x,y,z) = ("<<
            //  ((Candidate*)candUp)->Position.X() <<","<<
            //  ((Candidate*)candUp)->Position.Y() <<","<<
            //  ((Candidate*)candUp)->Position.Z() <<")"<<endmsg;
            
          }
          
          if (stage == 0) {
            isAlreadyContained = m_particleContainer->containedObject(PartKey)!=0;
          }
          else {
            isAlreadyContained = m_simpleParticleContainer->containedObject(PartKey) != 0;
          }
          if(isAlreadyContained){break;}
        }
        if(isAlreadyContained){
          if ( msgLevel(MSG::DEBUG) )debug()<<"candidate already written. moving on"<<endmsg;
          continue;          
        }
        
      }//end status on mother
      
      particle = (Candidate*)mother;
      first_particle = (Candidate*)mother->GetCandidates()->First();
      //if ( msgLevel(MSG::DEBUG) ) debug()<<"status = of the first particle: "<<first_particle->Status<<endmsg;
      const TLorentzVector &momentum = particle->Momentum;
      //Convert delphes particles into MCParticles
      auto part = std::make_unique<LHCb::MCParticle>();
      LHCb::ParticleID partID;
      partID.setPid(particle->PID);
      part->setParticleID(partID);
      Gaudi::LorentzVector m;
      Double_t modP2 = TMath::Power(momentum.Px(),2) + TMath::Power(momentum.Py(),2) + TMath::Power(momentum.Pz(),2); 
      Double_t en = TMath::Sqrt(modP2 + TMath::Power(particle->Mass,2));

      m.SetPxPyPzE(momentum.X(),momentum.Y(),momentum.Z(),momentum.T());
      const TLorentzVector &position = particle->Position;
      const TLorentzVector &first_position = first_particle->Position;
      
      // part->setMomentum(m);
      
      size_t key_rec = particle->M1;
      

      Gaudi::XYZPoint point;
      if (stage == 0) {
        part->setMomentum(m);
        point.SetXYZ(position.X(),
                     position.Y(),
                     position.Z());
      }
      else if (stage == 1) {
        // position.X(), position.Y()
        x = position.X();
        y = position.Y();
        // x = 3778.4; // ECAL_BorderX = 3878.4, ECAL_BorderY = 3151.2
        // y = 3051.2;

        x_module = static_cast<int>(std::trunc((x + ECAL_BorderX) / module_pitch));
        y_module = static_cast<int>(std::trunc((y + ECAL_BorderY) / module_pitch));

        if (x_module < 0 || x_module > num_x_modules || y_module < 0 || y_module > num_y_modules) {
            if (msgLevel(MSG::DEBUG)) debug() << "The hit is outside of the ECAL" << endmsg;
            continue;
        }

        if (ecal_modules[y_module][x_module] == 0) {
            if (msgLevel(MSG::DEBUG)) debug() << "The hit is in the area around the beam pipe" << endmsg;
            continue;
        }

        if (msgLevel(MSG::DEBUG)) {
            debug()  << "The hit is in the module type=" << ecal_modules[y_module][x_module]
            << " with coordinates: y_module=" << y_module << ", x_module=" << x_module << endmsg;
        }

        if (ecal_modules[y_module][x_module] == 1) {
            x = static_cast<double>(x_module) * module_pitch - ECAL_BorderX + cell_pitch[1] / 2.0;
            y = static_cast<double>(y_module) * module_pitch - ECAL_BorderY + cell_pitch[1] / 2.0;

        }
        else if (ecal_modules[y_module][x_module] == 2) {
            x = static_cast<double>(x_module) * module_pitch - ECAL_BorderX
                + std::trunc((x - static_cast<double>(x_module) * module_pitch + ECAL_BorderX) / cell_pitch[2]) 
                * cell_pitch[2]
                + cell_pitch[2] / 2.0;

            y = static_cast<double>(y_module) * module_pitch - ECAL_BorderY
                + std::trunc((y - static_cast<double>(y_module) * module_pitch + ECAL_BorderY) / cell_pitch[2])
                * cell_pitch[2]
                + cell_pitch[2] / 2.0;
        }
        else if (ecal_modules[y_module][x_module] == 3) {
            x = static_cast<double>(x_module) * module_pitch - ECAL_BorderX
                + std::trunc((x - static_cast<double>(x_module) * module_pitch + ECAL_BorderX) / cell_pitch[3])
                * cell_pitch[3]
                + cell_pitch[3] / 2.0;

            y = static_cast<double>(y_module) * module_pitch - ECAL_BorderY
                + std::trunc((y - static_cast<double>(y_module) * module_pitch + ECAL_BorderY) / cell_pitch[3])
                * cell_pitch[3]
                + cell_pitch[3] / 2.0;
        }                

        // m.SetXYZT(x, y, position.Z(), position.T());
        part->setMomentum(m);

        point.SetXYZ(x, y, position.Z());
      }
      
      auto EndVtx = std::make_unique<LHCb::MCVertex>();
      EndVtx->setPosition(point);
      EndVtx->setTime(position.T());
      EndVtx->setType( LHCb::MCVertex::DecayVertex);
      //info()<<"dumping end vertex"<<endmsg;
      //EndVtx->fillStream(std::cout);
      
      Double_t eta = part->pseudoRapidity();

      Gaudi::XYZPoint first_point;
      first_point.SetXYZ(first_position.X(),
             first_position.Y(),
             first_position.Z());

      auto OrigVtx = std::make_unique<LHCb::MCVertex>();
      OrigVtx->setPosition(first_point);
      OrigVtx->setTime(first_position.T());    
      if(std::find(generated_PV_barcodes.begin(), generated_PV_barcodes.end(), candidate->M2) != generated_PV_barcodes.end()){
        //if ( msgLevel(MSG::DEBUG) ) debug()<<"setting pv at the origin vertex insertion"<<endmsg;
        
        OrigVtx->setType( LHCb::MCVertex::ppCollision);
      }
      else{
        OrigVtx->setType( LHCb::MCVertex::GenericInteraction);
      }
      
      //if ( msgLevel(MSG::DEBUG) ) debug()<<"testing final PV->isPrimary()"<<OrigVtx->isPrimary()<<endmsg;
       
      if ( msgLevel(MSG::DEBUG) )
      {  
        debug()
          <<particle->PID<<std::setw(12)
          <<particle->Status<<std::setw(12)
          <<particle->Momentum.Px()<<std::setw(12)
          <<particle->Momentum.Py()<<std::setw(12)
          <<particle->Momentum.Pz()<<std::setw(12)
          <<particle->Mass<<std::setw(12)
          <<en<<std::setw(12)
          <<particle->Position.X()<<std::setw(12)
          <<particle->Position.Y()<<std::setw(12)
          <<particle->Position.Z()<<std::setw(12)
          <<position.T()<<std::setw(12)
          <<first_particle->Position.X()<<std::setw(12)
          <<first_particle->Position.Y()<<std::setw(12)
          <<first_particle->Position.Z()<<std::setw(12)
          <<first_position.T()<<std::setw(12)
          <<particle->M1<<std::setw(12)
          <<eta
          <<endmsg ;
      }
      
      
      // part->setOriginVertex(EndVtx);
      part->addToEndVertices(EndVtx.get());
      part->setOriginVertex(OrigVtx.get());

      if (stage == 0) {
        m_verticesContainer->insert(OrigVtx.release());
        m_verticesContainer->insert(EndVtx.release());
      }
      else if (stage == 1) {
        m_verticesContainer->insert(OrigVtx.release());
        m_verticesContainer->insert(EndVtx.release());               
      }    

      //info()<<"particle endvertices size = "<<part->endVertices().size()<<endmsg;
      
      
      if (particle->M1 > 0)
      {
        if ( msgLevel(MSG::DEBUG) )debug()<<"putting particle " << key_rec << " into the container"<<endmsg;
          if (stage == 0) {
            m_particleContainer->insert(part.release(),key_rec);
          }
          else if (stage == 1) {
            m_simpleParticleContainer->insert(part.release(), key_rec);
          }
      }
      //else
      //{
      //  Warning("A particle with PID "+std::to_string(particle->PID) + " was generated, but ignored by Delphes.");
      //  return StatusCode::FAILURE;
      //}
    }
  } // end of loop over containers

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode DelphesAlg::finalize() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;
  
  m_modularDelphes->FinishTask();
  
  return GaudiAlgorithm::finalize();  // must be called after all other actions
}
 
//=============================================================================
HepMC::GenVertex* DelphesAlg::primaryVertex
(const HepMC::GenEvent* genEvent) const {
  HepMC::GenVertex* result = nullptr;  
  // First method, get the beam particle and use the decay vertex if it exists.
  if (genEvent->valid_beam_particles()) {
    HepMC::GenParticle* P = genEvent->beam_particles().first;
    HepMC::GenVertex*   V = P->end_vertex();
    if (V) result = V;    
    else error() << "The beam particles have no end vertex!" << endmsg;
  // Second method, use the singal vertex stored in HepMC.
  } else if ( 0 != genEvent -> signal_process_vertex() ) {
    HepMC::GenVertex* V = genEvent->signal_process_vertex();
    result = V;    
  // Third method, take production/end vertex of the particle with barcode 1.
  } else {
    HepMC::GenParticle* P = genEvent->barcode_to_particle(1);
    HepMC::GenVertex*   V = 0;
    if (P) {
      V = P->production_vertex();
      if (V) result = V;      
      else {
        V = P->end_vertex();
        if (V) result = V;        
        else error() << "The first particle has no production vertex and "
         << "no end vertex !" << endmsg ;
      }
    } else error() << "No particle with barcode equal to 1!" << endmsg;
  }
  return result;
  }