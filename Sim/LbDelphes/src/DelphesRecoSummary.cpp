// Local 
#include "DelphesRecoSummary.h"

//MCParticle 
#include "Event/ProtoParticle.h"

//Gaudi
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
#include "GaudiKernel/AlgFactory.h" 

// STL
#include <vector>
#include <algorithm>
#include <string>

// ROOT 
#include <TFile.h>

DECLARE_COMPONENT ( DelphesRecoSummary )
//================================================================================
// Constructor 
//================================================================================
DelphesRecoSummary::DelphesRecoSummary( const std::string& name, ISvcLocator* pSvcLocator )
  : GaudiAlgorithm ( name , pSvcLocator )
{
}

//================================================================================
// Destructor 
//================================================================================
//DelphesRecoSummary::~DelphesRecoSummary()
//{
//}

//================================================================================
// Initialize 
//================================================================================
StatusCode DelphesRecoSummary::initialize()
{
  if (m_nTracks_histFile != "")
  {
    TFile *f = TFile::Open  (static_cast<std::string>(m_nTracks_histFile).c_str()); 
    f->GetObject (static_cast<std::string>(m_nTracks_histName).c_str(), m_hist_nTracks);
    m_hist_nTracks->SetDirectory ( 0 ); 
    f->Close(); 
    delete f; 
  }

  return StatusCode::SUCCESS;
}

//================================================================================
// Execute 
//================================================================================
StatusCode DelphesRecoSummary::execute   ()
{
  auto *summary = new LHCb::RecSummary(); 
  put ( summary, m_summaryLoc ); 

  if (m_hist_nTracks != NULL) 
    summary->addInfo ( LHCb::RecSummary::nTracks, m_hist_nTracks->GetRandom() ); 
  else 
  {
    LHCb::ProtoParticles *protos = getIfExists<LHCb::ProtoParticles> (
                                                  m_protoParticlesLocation 
                                                ); 
    summary->addInfo ( LHCb::RecSummary::nTracks, protos ? protos->size() : 0 );  
  }

  return StatusCode::SUCCESS;
}


//================================================================================
// Finalize 
//================================================================================
StatusCode DelphesRecoSummary::finalize  ()
{
  delete m_hist_nTracks; 
  return StatusCode::SUCCESS;
}

