#pragma once
// Include files 
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include "Event/Particle.h"
#include "Event/RecSummary.h"
#include "GaudiKernel/RndmGenerators.h"//for MC integration
#include "GaudiKernel/Property.h"//for MC integration

#include <memory>

// from ROOT
#include <TH1D.h>

/** @class DelphesRecoSummary DelphesRecoSummary.h
 *
 *  @author Lucio Anderlini
 *  @date   2019-02-20  File created
 */
class DelphesRecoSummary : public GaudiAlgorithm 
{
  public: 
    /// Standard constructor
    DelphesRecoSummary( const std::string& name, ISvcLocator* pSvcLocator );

//    virtual ~DelphesRecoSummary( ); ///< Destructor

    virtual StatusCode initialize();    ///< Algorithm initialization
    virtual StatusCode execute   ();    ///< Algorithm execution
    virtual StatusCode finalize  ();    ///< Algorithm finalization

  private:

////////////////////////////////////////////////////////////////////////////////
// Configuration of the TES location                                          //
////////////////////////////////////////////////////////////////////////////////
    Gaudi::Property<std::string> m_summaryLoc {this,
      "RecSummaryLocation", LHCb::RecSummaryLocation::Default,
      "Output TES location for the simulated RecSummary"}; 

    Gaudi::Property<std::string> m_protoParticlesLocation {this,
      "MCFastProtoParticles", LHCb::ProtoParticleLocation::Charged,
      "Location from which to read the ProtoParticles. "}; 

////////////////////////////////////////////////////////////////////////////////
// Configuration of the input distributions                                   //
////////////////////////////////////////////////////////////////////////////////
    Gaudi::Property<std::string> m_nTracks_histFile {this, 
      "nTracksHistogramFile", "",
      "Histogram defining the distribution for nTracks format: path to TFile"
    };
    Gaudi::Property<std::string> m_nTracks_histName {this, 
      "nTracksHistogramName", "",
      "Histogram defining the distribution for nTracks format: name of TObject"
    };

  private: // methods 
    TH1D *m_hist_nTracks{nullptr}; 
};



