#ifndef DELPHESALG_H 
#define DELPHESALG_H 1

// Include files 
// from Gaudi
#include "GaudiAlg/GaudiAlgorithm.h"
#include "TMath.h"
#include "Math/VectorUtil.h"

#include "TLorentzVector.h"
#include "TVector3.h"

class ExRootConfReader ;
class Delphes ;
class DelphesFactory ;
class TObjArray ;

/** @class DelphesAlg DelphesAlg.h
 *
 *  Interface to DELPHES packages
 *
 *  @author Patrick Robbe
 *  @date   2015-01-19
 */
class DelphesAlg : public GaudiAlgorithm {
public: 
  DelphesAlg( const std::string& name, ISvcLocator* pSvcLocator );  
  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute   () override;    ///< Algorithm execution
  StatusCode finalize  () override;    ///< Algorithm finalization
  /* virtual StatusCode fill( const LHCb::Particle* */
  /*                          , const LHCb::Particle* */
  /*                          , const std::string& */
  /*                          , Tuples::Tuple& ); */
  HepMC::GenVertex* primaryVertex(const HepMC::GenEvent* genEvent) const;
  
protected:

private:
  Long64_t key_gen; //sequential key to give to candidates
  
  Gaudi::Property<std::string> m_generationLocation{this,
      "HepMCEventLocation",
      LHCb::HepMCEventLocation::Default,
      "Location to read the HepMC event."};///< Location in TES of input HepMC events.
  Gaudi::Property<std::string> m_particles{this,
      "MCFastParticleLocation",
      "/Event/MCFast/MCParticles",
      "Location to write smeared MCParticles."}; ///< Location in TES of output smeared MCParticles.
  Gaudi::Property<std::string> m_vertices{this,
      "MCFastVerticesLocation",
      "/Event/MCFast/MCVertices",
      "Location to write smeared MCVertices."}; ///< Location in TES of output smeared MCVertices.
  Gaudi::Property<std::string> m_simpleParticles{this,
      "MCFastSimpleParticleLocation",
      "/Event/MCFast/MCSimpleParticles",
      "Location to write smeared simple MCParticles."}; ///< Location in TES of output smeared MCParticles.
  Gaudi::Property<std::string> m_generatedParticles{this,
      "MCParticleLocation",
      LHCb::MCParticleLocation::Default,
      "Location to write generated MCParticles."}; //< Location in TES of output generated MCParticles.
  Gaudi::Property<std::string> m_generatedVertices{this,
      "MCVerticesLocation",
      LHCb::MCVertexLocation::Default,
      "Location to write generated MCVertices."};  //< Location in TES of output generated MCVertices.
  Gaudi::Property<std::string> m_LHCbDelphesCard{this,
      "LHCbDelphesCardLocation",
      "./delphes_card.tcl",
      "Location of the Delphes Card for specific LHCb Implementation"};//< location of the file delphes_card_LHCb.tcl
  Gaudi::Property<std::string> m_track_location{this,
      "DelphesTrackLocation",
      "TrackMerger/tracks",
      "Location of the merged track (and neutral) candidates"};//< location of tracks as defined from Delphes Card.

  //ExRootConfReader * m_confReader{nullptr};
  //Delphes * m_modularDelphes{nullptr};
  std::unique_ptr<ExRootConfReader> m_confReader;
  std::unique_ptr<Delphes> m_modularDelphes;
  
  DelphesFactory * m_Delphes{nullptr};
  TObjArray * m_allParticleOutputArray{nullptr};
  TObjArray * m_stableParticleOutputArray{nullptr};
  TObjArray * m_partonOutputArray{nullptr};
  
  const char *fGeometryData;
  const std::size_t num_y_modules = 52;
  const std::size_t num_x_modules = 64;
  int ecal_modules[52][64]; // y vs x
  double res_m1, res_m2, res_m3;
  double res_m[3];

};
#endif // DELPHESALG_H
