// Local 
#include "DelphesParticleId.h"

//MCParticle 
#include "Event/MCParticle.h"

//Gaudi
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

// STL
#include <vector>
#include <algorithm>
#include <cstdlib>

DECLARE_COMPONENT ( DelphesParticleId )
//================================================================================
// Constructor 
//================================================================================
DelphesParticleId::DelphesParticleId( const std::string& name, ISvcLocator* pSvcLocator )
  : GaudiAlgorithm ( name , pSvcLocator )
{
}

//================================================================================
// Destructor 
//================================================================================
DelphesParticleId::~DelphesParticleId()
{
}

//================================================================================
// Initialize 
//================================================================================
StatusCode DelphesParticleId::initialize()
{
  LHCb::IParticlePropertySvc*  ppSvc = 
        this -> template svc<LHCb::IParticlePropertySvc> 
          ( "LHCb::ParticlePropertySvc" , true) ;

  // input validation 
  for (auto& [partName, ganDir] : m_rich_gan)
    if (!ppSvc->find(partName))
    {
      error() << "Particle " << partName << " defined for Rich GANs not valid." << endmsg;
      return StatusCode::FAILURE; 
    }

  for (auto& [partName, ganDir] : m_muon_gan)
    if (!ppSvc->find(partName))
    {
      error() << "Particle " << partName << " defined for Muon GANs not valid." << endmsg;
      return StatusCode::FAILURE; 
    }

  for (auto& [partName, ganDir] : m_isMuon_mlp)
    if (!ppSvc->find(partName))
    {
      error() << "Particle " << partName << " defined for isMuon MLPs not valid." << endmsg;
      return StatusCode::FAILURE; 
    }


  // Register the neural networks associating Tensors to the input and output layers 
  for (auto& [partName, ganDir] : m_rich_gan)
    m_richNN [ ppSvc->find(partName)->pdgID().pid() ] = std::unique_ptr < gtf::Predictor > (
          new gtf::Predictor (ganDir, {m_rich_input}, {m_rich_output}) 
        ); 

  for (auto& [partName, ganDir] : m_muon_gan)
  {
    auto isMuonNN = m_isMuon_mlp.find ( partName );
    if (isMuonNN == m_isMuon_mlp.end()) 
      m_muonNN [ ppSvc->find(partName)->pdgID().pid() ] = std::make_pair ( 
        std::unique_ptr < gtf::Predictor > ( nullptr ),
        std::unique_ptr < gtf::Predictor > (
            new gtf::Predictor (ganDir, {m_muon_input}, {m_muon_output}) 
        )
      ); 
    else 
      m_muonNN [ ppSvc->find(partName)->pdgID().pid() ] = std::make_pair ( 
        std::unique_ptr < gtf::Predictor > (
            new gtf::Predictor (isMuonNN->second, {m_isMuon_input}, {m_isMuon_output}) 
        ),
        std::unique_ptr < gtf::Predictor > (
            new gtf::Predictor (ganDir, {m_muon_input}, {m_muon_output}) 
        )
      ); 
  }

  for (auto& [partName, ganDir] : m_isMuon_mlp)
    if (m_muon_gan.find ( partName ) == m_isMuon_mlp.end()) // if not already filled
      m_muonNN [ ppSvc->find(partName)->pdgID().pid() ] = std::make_pair ( 
        std::unique_ptr < gtf::Predictor > (
            new gtf::Predictor (ganDir, {m_isMuon_input}, {m_isMuon_output}) 
        ),
        std::unique_ptr < gtf::Predictor > ( nullptr )
      ); 

  return StatusCode::SUCCESS;
}

//================================================================================
// Execute 
//================================================================================
StatusCode DelphesParticleId::execute   ()
{
  // Evaluate the Rich GANs
  for (auto& [ mcId, gan ]: m_richNN) 
    evalRich ( getKeysOfParticles(mcId), *gan ); 

  // Evaluate the Muon GANs
  for (auto& [ mcId, nets ]: m_muonNN) 
      evalMuon ( getKeysOfParticles(mcId), nets.first.get(), nets.second.get() ); 

  return StatusCode::SUCCESS;
}


//================================================================================
// Finalize 
//================================================================================
StatusCode DelphesParticleId::finalize  ()
{
  return StatusCode::SUCCESS;
}


//================================================================================
// getKeysOfParticles
//================================================================================
std::vector <int> DelphesParticleId::getKeysOfParticles (int mcId)
{
  std::vector < int > keys; 
  auto inputTes = get<LHCb::MCParticles>(m_input_particles); 

  for (auto& p : *inputTes)
    if (p->particleID().pid() == mcId)
      keys.push_back ( p->key() ); 

  return keys; 
}


//================================================================================
// evalRich
//================================================================================
StatusCode DelphesParticleId::evalRich (std::vector<int> keys, 
                                       gtf::Predictor& gan )
{
  if (keys.size() == 0) return StatusCode::SUCCESS; 

  auto inputTes = get<LHCb::MCParticles>(m_input_particles); 

  // Creates vectors for p and eta
  std::vector<t_RichFloat> p, eta; 
  std::transform (keys.begin(), keys.end(), std::back_inserter(p),
      [&](int key)->t_RichFloat { return inputTes->object(key)->p(); } ); 

  std::transform (keys.begin(), keys.end(), std::back_inserter(eta),
      [&](int key)->t_RichFloat { return inputTes->object(key)->pseudoRapidity(); } ); 

  // Creates a vector for nTracks 
  auto *summary = getIfExists<LHCb::RecSummary> ( m_recsummary_location ); 
  size_t nTrk = 0;
  if (summary) nTrk = summary->info ( LHCb::RecSummary::nTracks, 0. );
  std::vector<t_RichFloat> nTracks ( p.size(), nTrk ); 

  // Converts the vectors to tensors 
  const size_t nRichVars = 5; 
  auto tIn = gtf::TensorD::zip ( {p, eta, nTracks} ); 
  auto tOut = gtf::TensorD ( { keys.size(), nRichVars } ); 
  
  // Evaluates the neural network 
  gan . exec ( { &tIn }, { &tOut } ); 

  // Converts the output tensor back into MuonLL object 
  auto richPid = get<LHCb::RichPIDs> (m_rich_pid); 
  for (size_t iEntry = 0; iEntry < keys.size(); ++iEntry)
  {
    LHCb::RichPID* rpid = richPid->object ( keys[iEntry ] ); 
    rpid->setBestParticleID (Rich::Pion);
    rpid->setParticleDeltaLL(Rich::Pion, 0.); 

    debug() << "nTracks: " << nTrk << endmsg; 
    debug() << "RichGan: " << tOut[iEntry] << endmsg; 

    rpid->setParticleDeltaLL(Rich::Electron,        tOut[{iEntry,0}]  ); 
    rpid->setParticleDeltaLL(Rich::Kaon,            tOut[{iEntry,1}]  ); 
    rpid->setParticleDeltaLL(Rich::Muon,            tOut[{iEntry,2}]  ); 
    rpid->setParticleDeltaLL(Rich::Proton,          tOut[{iEntry,3}]  ); 
    rpid->setParticleDeltaLL(Rich::BelowThreshold,  tOut[{iEntry,4}]  ); 
    rpid->setParticleDeltaLL(Rich::Deuteron,        -99999. ); 
  }

  return StatusCode::SUCCESS;
}

//================================================================================
// evalMuon
//================================================================================
StatusCode DelphesParticleId::evalMuon (std::vector<int> keys, 
                                       gtf::Predictor* mlp, 
                                       gtf::Predictor* gan )
{
  debug()<<"evalMuon is called " << endmsg; 
  if (keys.size() == 0) return StatusCode::SUCCESS; 

  auto inputTes = get<LHCb::MCParticles>(m_input_particles); 

  // Creates vectors for p and eta
  std::vector<t_MuonFloat> p, eta; 
  std::transform (keys.begin(), keys.end(), std::back_inserter(p),
      [&](int key)->t_MuonFloat { return log(inputTes->object(key)->p()); } ); 

  std::transform (keys.begin(), keys.end(), std::back_inserter(eta),
      [&](int key)->t_MuonFloat { return inputTes->object(key)->pseudoRapidity(); } ); 

  // Creates a vector for nTracks 
  auto *summary = getIfExists<LHCb::RecSummary> ( m_recsummary_location ); 
  size_t nTrk = 0;
  if (summary) nTrk = summary->info ( LHCb::RecSummary::nTracks, 0. );
  std::vector<t_MuonFloat> nTracks ( p.size(), nTrk ); 

  // Converts the vectors to tensors 
  const size_t nMuonVars = 2; 
  auto tMuonIn = gtf::TensorF::zip ( {p, eta, nTracks} ); 
  auto tMuonOut = gtf::TensorF ( { p.size(), nMuonVars } ); 

  auto tisMuonIn = gtf::TensorF::zip ( {p, eta, nTracks} ); 
  auto tisMuonOut = gtf::TensorF ( { p.size() } ); 
  
  // Evaluates the neural network 
  if (mlp) mlp -> exec ( { &tisMuonIn }, { &tisMuonOut } ); 
  if (gan) gan -> exec ( { &tMuonIn }, { &tMuonOut } ); 

  // Converts the output tensor back into MuonLL object 
  auto muonPid = get<LHCb::MuonPIDs> (m_muon_pid); 
  for (size_t iEntry = 0; iEntry < keys.size(); ++iEntry)
  {
    const int key = keys[iEntry]; 
    bool isMuon = inputTes->object(key)->p() > 3e3;
    if (mlp)
    {
      debug()<<"isMuonMlp: " << tisMuonOut [iEntry] << endmsg; 
      isMuon &= (float(rand()) / RAND_MAX) < tisMuonOut[gtf::Indices{iEntry}] ? 1 : 0; 
    }

    muonPid->object (key)->setIsMuonLoose (isMuon);
    muonPid->object (key)->setIsMuon (isMuon);

    if (isMuon && gan) 
    {
      debug()<<"MuonGan: " << tMuonOut [iEntry] << endmsg; 
      muonPid->object (key)->setMuonLLMu ( tMuonOut[{iEntry,0}] );
      muonPid->object (key)->setMuonLLBg ( tMuonOut[{iEntry,1}] );
    }
  }

  return StatusCode::SUCCESS;
}
