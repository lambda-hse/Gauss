// Include files 

 // from Gaudi
#include "GaudiKernel/AlgFactory.h" 
#include "GaudiKernel/ToolFactory.h"
#include "GaudiAlg/Tuple.h"
#include "GaudiAlg/TupleObj.h"
#include "GaudiKernel/DeclareFactoryEntries.h"
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/IRndmEngine.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/IDataManagerSvc.h"
#include "GaudiAlg/GaudiTupleAlg.h"
// Event.
#include "Event/MCHeader.h"
#include "Event/MCParticle.h"
#include "Event/Particle.h"
// from LHCb
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

// local
#include "DelphesTuple.h"
#include "TLorentzVector.h"

//-----------------------------------------------------------------------------
// Implementation file for class : DelphesTuple
//
// 2015-10-20 : Benedetto Gianluca Siddi
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
//DECLARE_ALGORITHM_FACTORY( DelphesTuple )
DECLARE_COMPONENT( DelphesTuple )


//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
DelphesTuple::DelphesTuple( const std::string& name,
                          ISvcLocator* pSvcLocator)
: GaudiTupleAlg ( name , pSvcLocator ),
  tupleGen(0),
  tupleRec(0),
  tupleSimple(0),
  tupleRes(0),
  tupleSimpleRes(0),
  tupleResPhoton(0)
{
  declareProperty("MCFastParticleLocation",
                  m_particles = "/Event/MCFast/MCParticles",
                  "Location to write smeared MCParticles.");  
 
  declareProperty("MCFastVerticesLocation",
                  m_vertices = "/Event/MCFast/MCVertices",
                  "Location to write smeared MCVertices.");

  declareProperty("MCFastSimpleParticleLocation",
                   m_simpleParticles = "/Event/MCFast/MCSimpleParticles",
                   "Location to write smeared simple MCParticles.");
  
  declareProperty("MCVerticesLocation",
                  m_generatedVertices = LHCb::MCVertexLocation::Default,
                  "Location to write generated MCVertices.");
  
  declareProperty("MCParticleLocation",
                  m_generatedParticles = LHCb::MCParticleLocation::Default,
                  "Location to write generated MCParticles.");
  
  declareProperty("NeutralProtoLocation",
                  m_neutral_proto_location = LHCb::ProtoParticleLocation::Neutrals,
                  "location of neutral protoparticles");
  
}
//=============================================================================
// Destructor
//=============================================================================
DelphesTuple::~DelphesTuple() {} 

//=============================================================================
// Initialization
//=============================================================================
StatusCode DelphesTuple::initialize() {
  StatusCode sc = GaudiTupleAlg::initialize();
  // must be executed first
  if ( sc.isFailure() ) return sc;  // error printed already by GaudiAlgorithm
  tupleGen = nTuple("Gen", "gen_Tuple");
  info()<<"created Gen tuple"<<endmsg;
  tupleRec = nTuple("Rec", "rec_Tuple");
  info()<<"created Rec tuple"<<endmsg;
  tupleSimple = nTuple("Simple", "simple_Tuple");
  info() << "created Simple tuple" << endmsg;
  tupleRes = nTuple("Res", "res_Tuple");
  info()<<"created Res tuple"<<endmsg;
  tupleSimpleRes = nTuple("SimpleRes", "simple_res_Tuple");
  info() << "created SimpleRes tuple" << endmsg;
  //needed?
  //tupleGenPhoton = nTuple("GenPhoton", "gen_Photon_Tuple");
  //info()<<"created GenPhoton tuple"<<endmsg;
  //tupleRecPhoton = nTuple("RecPhoton", "gen_Photon_Tuple");
  //info()<<"created RecPhoton tuple"<<endmsg;
  tupleResPhoton = nTuple("ResPhoton","res_Photon_Tuple");
  info()<<"created ResPhoton tuple"<<endmsg;
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Initialize" << endmsg;
  return StatusCode::SUCCESS;

}

//=============================================================================
// Main execution
//=============================================================================
StatusCode DelphesTuple::execute() {
  
  
  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Execute" << endmsg;
  // Get the run and event number from the MC Header
  LHCb::MCHeader* evt = get<LHCb::MCHeader>( LHCb::MCHeaderLocation::Default,IgnoreRootInTES);
  
  //Get TES container
  LHCb::MCParticles *m_genParticleContainer =
    get<LHCb::MCParticles>(m_generatedParticles);

  LHCb::MCVertices  *m_genVerticesContainer = 
    get<LHCb::MCVertices>(m_generatedVertices);

  LHCb::MCParticles *m_particleContainer = 
    get<LHCb::MCParticles>(m_particles);

  LHCb::MCVertices  *m_verticesContainer = 
    get<LHCb::MCVertices>(m_vertices);

  LHCb::MCParticles *m_simpleParticleContainer =
    get<LHCb::MCParticles>(m_simpleParticles);
  
  LHCb::ProtoParticles* m_neutralProtos = get<LHCb::ProtoParticles>(m_neutral_proto_location);

  //containers for testing gen vs rec.
  //charged tracks.
  GenPions.clear();
  RecPions.clear();
  SimpleRecPions.clear();
  Pions.clear();
  SimplePions.clear();
  //photons
  GenPhoton.clear();
  ClusteredPhoton.clear();
  Photon.clear();  

  Int_t mother_ID, mother_PID;
  LHCb::MCParticles::const_iterator i;
  for( i=m_genParticleContainer->begin(); i != m_genParticleContainer->end(); i++ ) 
  {
    GenPions.insert(std::pair<Long64_t,LHCb::MCParticle*>((*i)->key(),(*i)));        

    LHCb::MCParticle *P = (*i);
    const LHCb::MCVertex &Vtx = P->originVertex();
    const LHCb::ParticleID &ID = P->particleID(); 
    Double_t x = Vtx.position().X();
    Double_t y = Vtx.position().Y();
    Double_t z = Vtx.position().Z();
    // Double_t x = P->momentum().X();
    // Double_t y = P->momentum().Y();
    // Double_t z = P->momentum().Z();
    Double_t R = TMath::Power(x,2) + TMath::Power(y,2);
    R = TMath::Sqrt(R);
    Double_t eta = P->pseudoRapidity();
    Double_t phi = P->momentum().Phi();
    Double_t theta = P->momentum().Theta();
    Double_t px = P->momentum().Px();
    Double_t py = P->momentum().Py();
    Double_t pz = P->momentum().Pz();
    Double_t theta_x = px/pz;
    Double_t theta_y = py/pz;
    Double_t p = P->momentum().P();
    Int_t Pid = ID.pid();

    
    if (P->originVertex()->mother() != NULL) {
        mother_PID = P->originVertex()->mother()->particleID().pid();
        mother_ID = P->originVertex()->mother()->key();
    }
    else {
        mother_PID = 0;
        mother_ID = -1;
    }

    tupleGen->column("runNumber",    evt->runNumber());
    tupleGen->column("eventNumber",  evt->evtNumber());
    tupleGen->column("mother_ID", mother_ID);
    tupleGen->column("mother_PID", mother_PID);
    tupleGen->column("p",p);
    tupleGen->column("theta",theta);
    tupleGen->column("thetaX",theta_x);
    tupleGen->column("thetaY",theta_y);
    tupleGen->column("x",x);
    tupleGen->column("y",y);
    tupleGen->column("z",z);
    tupleGen->column("px",px);
    tupleGen->column("py",py);
    tupleGen->column("pz",pz);
    tupleGen->column("phi",phi);
    tupleGen->column("eta",eta);
    tupleGen->column("pid",Pid);    
    tupleGen->write();
    //separate photons as well.
    if(abs(Pid)==22){
      if(msgLevel(MSG::DEBUG) ) debug()<<"got a photon with Z = "<< z<<endmsg;      
      GenPhoton.insert(std::pair<Long64_t, LHCb::MCParticle*>((*i)->key(),(*i)));
      if(msgLevel(MSG::DEBUG))debug()<<"inserted a photon with key"<<(*i)->key()<<", now adding the tuple info"<<endmsg;
      
      //tupleResPhoton->column("mc_calo_key",(*i)->key());
      //if(msgLevel(MSG::DEBUG) ) debug()<<"end verticies size = "<<P->endVertices().size()<<endmsg;
      
      //auto v = P->endVertices().back();
      //std::cout<<v<<std::endl;
        
      //   //tupleResPhoton->column("mc_vertex_key",Vtx.key());
      
      // tupleGenPhoton->column("mc_calo_e",p);
      // info()<<"1"<<endmsg;
      // tupleGenPhoton->column("mc_calo_px",px);
      // info()<<"2"<<endmsg;
      // tupleGenPhoton->column("mc_calo_py",py);
      // info()<<"3"<<endmsg;
      // tupleGenPhoton->column("mc_calo_pz",pz);
      // info()<<"4"<<endmsg;
      // info()<<"looking at endvertices"<<endmsg;
      // auto vertices = P->endVertices();
      // info()<<"got "<<vertices.size()<<"vertices"<<endmsg;
      
      // tupleGenPhoton->column("mc_calo_x",P->endVertices()[0]->position().x());
      // info()<<"5"<<endmsg;
      // tupleGenPhoton->column("mc_calo_y",P->endVertices()[0]->position().y());
      // info()<<"6"<<endmsg;
      // tupleGenPhoton->column("mc_calo_z",P->endVertices()[0]->position().z());
      // info()<<"7"<<endmsg;
      // tupleGenPhoton->write();
      // info()<<"8"<<endmsg;
    }
      
  }
  
  // info()<<"GenSize: "<<GenPions.size()<<endmsg;

  for( i=m_particleContainer->begin(); i != m_particleContainer->end(); i++ ) 
  { 
    RecPions.insert(std::pair<Long64_t,LHCb::MCParticle*>((*i)->key(),(*i)));
    LHCb::MCParticle *P = (*i);
    const LHCb::MCVertex *Vtx = P->endVertices()[0];
    const LHCb::ParticleID &ID = P->particleID();
    
    Double_t x = Vtx->position().X();
    Double_t y = Vtx->position().Y();
    Double_t z = Vtx->position().Z();
    // Double_t x = P->momentum().X();
    // Double_t y = P->momentum().Y();
    // Double_t z = P->momentum().Z(); 
    Double_t eta = P->pseudoRapidity();
    Double_t phi = P->momentum().Phi();
    Double_t theta = P->momentum().Theta();
    Double_t R = TMath::Power(x,2) + TMath::Power(y,2);
    Double_t px = P->momentum().Px();
    Double_t py = P->momentum().Py();
    Double_t pz = P->momentum().Pz();
    Double_t theta_x = px/pz;
    Double_t theta_y = py/pz;
    Double_t p = P->momentum().P();
    Int_t Pid = ID.pid();
    
    
    R = TMath::Sqrt(R);
    tupleRec->column( "runNumber",    evt->runNumber() );
    tupleRec->column( "eventNumber",  evt->evtNumber() );
    tupleRec->column("p",p);
    tupleRec->column("theta",theta);
    tupleRec->column("thetaX",theta_x);
    tupleRec->column("thetaY",theta_y);
    tupleRec->column("x",x);
    tupleRec->column("y",y);
    tupleRec->column("z",z);
    tupleRec->column("px",px);
    tupleRec->column("py",py);
    tupleRec->column("pz",pz);
    tupleRec->column("phi",phi);
    tupleRec->column("eta",eta);
    tupleRec->column("pid",Pid);
    tupleRec->write();

  }

  for (i = m_simpleParticleContainer->begin(); i != m_simpleParticleContainer->end(); i++)
  {
      SimpleRecPions.insert(std::pair<Long64_t, LHCb::MCParticle*>((*i)->key(), (*i)));
      LHCb::MCParticle *P = (*i);
      const LHCb::MCVertex *Vtx = P->endVertices()[0];
      const LHCb::ParticleID &ID = P->particleID();

      Double_t x = Vtx->position().X();
      Double_t y = Vtx->position().Y();
      Double_t z = Vtx->position().Z();
      Double_t eta = P->pseudoRapidity();
      Double_t phi = P->momentum().Phi();
      Double_t theta = P->momentum().Theta();
      Double_t R = TMath::Power(x, 2) + TMath::Power(y, 2);
      Double_t px = P->momentum().Px();
      Double_t py = P->momentum().Py();
      Double_t pz = P->momentum().Pz();
      Double_t theta_x = px / pz;
      Double_t theta_y = py / pz;
      Double_t p = P->momentum().P();
      Int_t Pid = ID.pid();

      //if (P->originVertex()->mother() != NULL) {
      //    mother_PID = P->originVertex()->mother()->particleID().pid();
      //    mother_ID = P->originVertex()->mother()->key();
      //}
      //else {
      //    mother_PID = 0;
      //    mother_ID = -1;
      //}

      R = TMath::Sqrt(R);
      tupleSimple->column("runNumber", evt->runNumber());
      tupleSimple->column("eventNumber", evt->evtNumber());
      //tupleSimple->column("mother_ID", mother_ID);
      //tupleSimple->column("mother_PID", mother_PID);
      tupleSimple->column("p", p);
      tupleSimple->column("theta", theta);
      tupleSimple->column("thetaX", theta_x);
      tupleSimple->column("thetaY", theta_y);
      tupleSimple->column("x", x);
      tupleSimple->column("y", y);
      tupleSimple->column("z", z);
      tupleSimple->column("px", px);
      tupleSimple->column("py", py);
      tupleSimple->column("pz", pz);
      tupleSimple->column("phi", phi);
      tupleSimple->column("eta", eta);
      tupleSimple->column("pid", Pid);
      tupleSimple->write();

  }


  // info()<<"RecSize: "<<RecPions.size()<<endmsg;  
  std::map<Long64_t, LHCb::MCParticle*>::iterator it;

  for(it=GenPions.begin(); it!=GenPions.end(); ++it)
  {
    if(RecPions.find(it->first) != RecPions.end()) 
      Pions.insert(std::make_pair(it->first,std::make_pair(it->second,RecPions[it->first])));
  }

  for (it = GenPions.begin(); it != GenPions.end(); ++it)
  {
      if (SimpleRecPions.find(it->first) != SimpleRecPions.end())
          SimplePions.insert(std::make_pair(it->first, std::make_pair(it->second, SimpleRecPions[it->first])));
  }
  // info()<<"PionsSize: "<<Pions.size()<<endmsg;
  
  std::pair<LHCb::MCParticle*,LHCb::MCParticle*> particles;

  std::map<Long64_t, std::pair<LHCb::MCParticle*,LHCb::MCParticle*>>::iterator itp;

  LHCb::MCParticle *P1;
  LHCb::MCParticle *P2;
  LHCb::ParticleID *ID1;
  LHCb::ParticleID *ID2;
  Double_t Px1, Py1, Pz1, Pt1, x1, y1, z1, E1, M1;
  Double_t Px2, Py2, Pz2, Pt2, x2, y2, z2, E2, M2;
  Int_t Pid1, Pid2;
  for(itp=Pions.begin(); itp!=Pions.end(); ++itp)
  {
    particles = itp->second;
    P1 = (LHCb::MCParticle*)particles.first;
    P2 = (LHCb::MCParticle*)particles.second;
    const LHCb::ParticleID &ID1 = P1->particleID();
    const LHCb::ParticleID &ID2 = P2->particleID();
    const LHCb::MCVertex &Vtx1 = P1->originVertex();
    const LHCb::MCVertex *Vtx2 = P2->endVertices()[0];
    
    x1 = Vtx1.position().X();
    y1 = Vtx1.position().Y();
    z1 = Vtx1.position().Z();
    Pid1 = ID1.pid();
    Px1 = P1->momentum().Px();
    Py1 = P1->momentum().Py();
    Pz1 = P1->momentum().Pz();
    Pt1 = P1->momentum().Pt();
    E1 = P1->momentum().E();
    M1 = P1->momentum().M();

    Double_t eta1 = P1->pseudoRapidity();
    Double_t phi1 = P1->momentum().Phi();
    Double_t theta1 = P1->momentum().Theta();

    x2 = Vtx2->position().X();
    y2 = Vtx2->position().Y();
    z2 = Vtx2->position().Z();
    Pid2 = ID2.pid();
    Px2 = P2->momentum().Px();
    Py2 = P2->momentum().Py();
    Pz2 = P2->momentum().Pz();
    Pt2 = P2->momentum().Pt();
    E2 = P2->momentum().E();
    M2 = P2->momentum().M();
    Double_t p1 = P1->momentum().P();
    Double_t p2 = P2->momentum().P();

    // Double_t pidMother = P1->
    
    Double_t theta_x1 = Px1/Pz1;
    Double_t theta_y1 = Py1/Pz1;
    Double_t theta_x2 = Px2/Pz2;
    Double_t theta_y2 = Py2/Pz2;

    Double_t eta2 = P2->pseudoRapidity();
    Double_t phi2 = P2->momentum().Phi();
    Double_t theta2 = P2->momentum().Theta();

    if (P1->originVertex()->mother() != NULL) {
        mother_PID = P1->originVertex()->mother()->particleID().pid();
        mother_ID = P1->originVertex()->mother()->key();
    }
    else {
        mother_PID = 0;
        mother_ID = -1;
    }

    tupleRes->column( "runNumber", evt->runNumber() );
    tupleRes->column( "eventNumber", evt->evtNumber() );
    tupleRes->column("mother_ID", mother_ID);
    tupleRes->column("mother_PID", mother_PID);
    tupleRes->column("pDiff",p2-p1);
    tupleRes->column("thetaDiff",theta2-theta1);
    tupleRes->column("thetaXDiff",theta_x2-theta_x1);
    tupleRes->column("thetaYDiff",theta_y2-theta_y1);
    tupleRes->column("xDiff",x2-x1);
    tupleRes->column("yDiff",y2-y1);
    tupleRes->column("zDiff",z2-z1);
    tupleRes->column("pxDiff",Px2-Px1);
    tupleRes->column("pyDiff",Py2-Py1);
    tupleRes->column("pzDiff",Pz2-Pz1);
    tupleRes->column("phiDiff",phi2-phi1);
    tupleRes->column("etaDiff",eta2-eta1);
    tupleRes->column("pidDiff",Pid2-Pid1);

    tupleRes->column("pGen",p1);
    tupleRes->column("thetaGen",theta1);
    tupleRes->column("thetaXGen",theta_x1);
    tupleRes->column("thetaYGen",theta_y1);
    tupleRes->column("xGen",x1);
    tupleRes->column("yGen",y1);
    tupleRes->column("zGen",z1);
    tupleRes->column("pxGen",Px1);
    tupleRes->column("pyGen",Py1);
    tupleRes->column("pzGen",Pz1);
    tupleRes->column("phiGen",phi1);
    tupleRes->column("etaGen",eta1);
    tupleRes->column("pidGen",Pid1);

    tupleRes->column("pRec",p2);
    tupleRes->column("thetaRec",theta2);
    tupleRes->column("thetaXRec",theta_x2);
    tupleRes->column("thetaYRec",theta_y2);
    tupleRes->column("xRec",x2);
    tupleRes->column("yRec",y2);
    tupleRes->column("zRec",z2);
    tupleRes->column("pxRec",Px2);
    tupleRes->column("pyRec",Py2);
    tupleRes->column("pzRec",Pz2);
    tupleRes->column("phiRec",phi2);
    tupleRes->column("etaRec",eta2);
    tupleRes->column("pidRec",Pid2);
    // tupleRes->column("pidMother", pidMother);

    tupleRes->write();

  }

  for (itp = SimplePions.begin(); itp != SimplePions.end(); ++itp)
  {
      particles = itp->second;
      P1 = (LHCb::MCParticle*)particles.first;
      P2 = (LHCb::MCParticle*)particles.second;
      const LHCb::ParticleID &ID1 = P1->particleID();
      const LHCb::ParticleID &ID2 = P2->particleID();
      const LHCb::MCVertex &Vtx1 = P1->originVertex();
      const LHCb::MCVertex *Vtx2 = P2->endVertices()[0];

      x1 = Vtx1.position().X();
      y1 = Vtx1.position().Y();
      z1 = Vtx1.position().Z();
      Pid1 = ID1.pid();
      Px1 = P1->momentum().Px();
      Py1 = P1->momentum().Py();
      Pz1 = P1->momentum().Pz();
      Pt1 = P1->momentum().Pt();
      E1 = P1->momentum().E();
      M1 = P1->momentum().M();

      Double_t eta1 = P1->pseudoRapidity();
      Double_t phi1 = P1->momentum().Phi();
      Double_t theta1 = P1->momentum().Theta();

      x2 = Vtx2->position().X();
      y2 = Vtx2->position().Y();
      z2 = Vtx2->position().Z();
      Pid2 = ID2.pid();
      Px2 = P2->momentum().Px();
      Py2 = P2->momentum().Py();
      Pz2 = P2->momentum().Pz();
      Pt2 = P2->momentum().Pt();
      E2 = P2->momentum().E();
      M2 = P2->momentum().M();
      Double_t p1 = P1->momentum().P();
      Double_t p2 = P2->momentum().P();

      // Double_t pidMother = P1->

      Double_t theta_x1 = Px1 / Pz1;
      Double_t theta_y1 = Py1 / Pz1;
      Double_t theta_x2 = Px2 / Pz2;
      Double_t theta_y2 = Py2 / Pz2;

      Double_t eta2 = P2->pseudoRapidity();
      Double_t phi2 = P2->momentum().Phi();
      Double_t theta2 = P2->momentum().Theta();

      if (P1->originVertex()->mother() != NULL) {
          mother_PID = P1->originVertex()->mother()->particleID().pid();
          mother_ID = P1->originVertex()->mother()->key();
      }
      else {
          mother_PID = 0;
          mother_ID = -1;
      }

      tupleSimpleRes->column("runNumber", evt->runNumber());
      tupleSimpleRes->column("eventNumber", evt->evtNumber());
      tupleSimpleRes->column("mother_ID", mother_ID);
      tupleSimpleRes->column("mother_PID", mother_PID);
      tupleSimpleRes->column("pDiff", p2 - p1);
      tupleSimpleRes->column("thetaDiff", theta2 - theta1);
      tupleSimpleRes->column("thetaXDiff", theta_x2 - theta_x1);
      tupleSimpleRes->column("thetaYDiff", theta_y2 - theta_y1);
      tupleSimpleRes->column("xDiff", x2 - x1);
      tupleSimpleRes->column("yDiff", y2 - y1);
      tupleSimpleRes->column("zDiff", z2 - z1);
      tupleSimpleRes->column("pxDiff", Px2 - Px1);
      tupleSimpleRes->column("pyDiff", Py2 - Py1);
      tupleSimpleRes->column("pzDiff", Pz2 - Pz1);
      tupleSimpleRes->column("phiDiff", phi2 - phi1);
      tupleSimpleRes->column("etaDiff", eta2 - eta1);
      tupleSimpleRes->column("pidDiff", Pid2 - Pid1);

      tupleSimpleRes->column("pGen", p1);
      tupleSimpleRes->column("thetaGen", theta1);
      tupleSimpleRes->column("thetaXGen", theta_x1);
      tupleSimpleRes->column("thetaYGen", theta_y1);
      tupleSimpleRes->column("xGen", x1);
      tupleSimpleRes->column("yGen", y1);
      tupleSimpleRes->column("zGen", z1);
      tupleSimpleRes->column("pxGen", Px1);
      tupleSimpleRes->column("pyGen", Py1);
      tupleSimpleRes->column("pzGen", Pz1);
      tupleSimpleRes->column("phiGen", phi1);
      tupleSimpleRes->column("etaGen", eta1);
      tupleSimpleRes->column("pidGen", Pid1);

      tupleSimpleRes->column("pRec", p2);
      tupleSimpleRes->column("thetaRec", theta2);
      tupleSimpleRes->column("thetaXRec", theta_x2);
      tupleSimpleRes->column("thetaYRec", theta_y2);
      tupleSimpleRes->column("xRec", x2);
      tupleSimpleRes->column("yRec", y2);
      tupleSimpleRes->column("zRec", z2);
      tupleSimpleRes->column("pxRec", Px2);
      tupleSimpleRes->column("pyRec", Py2);
      tupleSimpleRes->column("pzRec", Pz2);
      tupleSimpleRes->column("phiRec", phi2);
      tupleSimpleRes->column("etaRec", eta2);
      tupleSimpleRes->column("pidRec", Pid2);

      tupleSimpleRes->write();

  }
  
  LHCb::ProtoParticles::const_iterator thingy;
  for(thingy=m_neutralProtos->begin(); thingy!=m_neutralProtos->end();thingy++){
    ClusteredPhoton.insert(std::make_pair((*thingy)->key(),(*thingy)));
    if(msgLevel(MSG::DEBUG))debug()<<"adding clustered photon with key "<<(*thingy)->key()<<endmsg;
    // tupleRecPhoton->column("energy",(*thingy)->calo().at(0)->e());
    // tupleRecPhoton->column("x",(*thingy)->calo().at(0)->position()->x());
    // tupleRecPhoton->column("y",(*thingy)->calo().at(0)->position()->y());
    // tupleRecPhoton->column("e",(*thingy)->calo().at(0)->e());
    //tupleRecPhoton->column("cov_xx"(*thingy)->calo().at(0)->covariance()
    // //tupleRecPhoton->column("recoKey",(*thingy)->key());    
    // tupleRecPhoton->write();
  }
  std::map<Long64_t,LHCb::MCParticle*>::iterator itPhoton;
  for(itPhoton = GenPhoton.begin(); itPhoton!=GenPhoton.end();itPhoton++){
    
    //match the photons by key
    //std::map<Long64_t,MCParticle*>::const_iterator match_val;
    
    auto itClusPhoton = std::find_if(ClusteredPhoton.begin(),ClusteredPhoton.end(),
                                 [&itPhoton](std::pair<Long64_t, LHCb::ProtoParticle*> element){                                  
                                       return element.first == (*itPhoton).first;});
    if(itClusPhoton!=ClusteredPhoton.end()){
      if(msgLevel(MSG::DEBUG))debug()<<"found matching gen Photon"<<(*itPhoton).first<<" <--> "<<(*itClusPhoton).first<<endmsg;      
      Photon.insert(std::make_pair(itPhoton->first,std::make_pair(itPhoton->second,itClusPhoton->second)));
    }
    
  }
  
  // for(itPhoton = GenPhoton.begin(); itPhoton!=GenPhoton.end(); itPhoton++){
  //   if(
  //      ClusteredPhoton.find_if() 
  //      != ClusteredPhoton.end()){
  //     Photon.insert(std::make_pair(itPhoton->first, std::make_pair(itPhoton->second,ClusteredPhoton[itPhoton])));
  //   }
  // }
  
  // //gen vs clustered photons matched. Write tuple.
  for( auto itResPhoton= Photon.begin();
       itResPhoton!= Photon.end();++itResPhoton){
    tupleResPhoton->column( "runNumber",    evt->runNumber() );
    tupleResPhoton->column( "eventNumber",  evt->evtNumber() );
    tupleResPhoton->column("photon_key",(*itResPhoton).first);//probably don't need this, but whatever
    //gen vs rec info
    tupleResPhoton->column("eGen",(*itResPhoton).second.first->momentum().P());
    tupleResPhoton->column("pxGen",(*itResPhoton).second.first->momentum().Px());
    tupleResPhoton->column("pyGen",(*itResPhoton).second.first->momentum().Py());
    tupleResPhoton->column("pzGen",(*itResPhoton).second.first->momentum().Pz());
    //take xyz at production, then propagate to the ecal face.
    double x0 = (*itResPhoton).second.first->originVertex()->position().x();
    double y0 = (*itResPhoton).second.first->originVertex()->position().y();
    double z0 = (*itResPhoton).second.first->originVertex()->position().z();
    double genTx = (*itResPhoton).second.first->momentum().Px()/(*itResPhoton).second.first->momentum().Pz();
    double genTy = (*itResPhoton).second.first->momentum().Py()/(*itResPhoton).second.first->momentum().Pz();
    tupleResPhoton->column("txGen",genTx);    
    tupleResPhoton->column("tyGen",genTy);
    tupleResPhoton->column("x0Gen",x0);
    tupleResPhoton->column("y0Gen",y0);
    tupleResPhoton->column("z0Gen",z0);
    //propagate to the final place
    //need x,y,z,tx,ty for 
    auto clus_pos = (*itResPhoton).second.second->calo().at(0)->position();
    double x = clus_pos->x();
    double y = clus_pos->y();
    double z  = clus_pos->z();
    double xGenCalo=x0+genTx*(z-z0);
    double yGenCalo=y0+genTy*(z-z0);
    tupleResPhoton->column("xGenCalo",xGenCalo);
    tupleResPhoton->column("yGenCalo",yGenCalo);
    tupleResPhoton->column("zGenCalo",z);
    
    // auto ev = (*itResPhoton).second.first->endVertices()[0];
    // info()<<"**************** checking end vertex in tuple *****************************"<<endmsg;
    // ev->fillStream(std::cout);
    // std::cout<<std::endl;
    // info()<<"**************************************************************************"<<endmsg;
    double ebase = (*itResPhoton).second.second->calo().at(0)->e()/sqrt(x*x+y*y+z*z);
    //taken from Melody Ravonel's Low Multiplicity reco. used in px,py,pz    
    tupleResPhoton->column("eRec",(*itResPhoton).second.second->calo().at(0)->e());
    tupleResPhoton->column("pxRec",ebase*x);    
    tupleResPhoton->column("pyRec",ebase*y);    
    tupleResPhoton->column("pzRec",ebase*z);
    tupleResPhoton->column("txRec",x/z);
    tupleResPhoton->column("tyRec",y/z);
    tupleResPhoton->column("xRec",x);
    tupleResPhoton->column("yRec",y);
    tupleResPhoton->column("zRec",z);
    
    tupleResPhoton->column("RecCovXX",clus_pos->covariance()(0,0));
    tupleResPhoton->column("RecCovYY",clus_pos->covariance()(1,1));
    tupleResPhoton->column("RecCovEE",clus_pos->covariance()(2,2));
    tupleResPhoton->column("RecCovXY",clus_pos->covariance()(0,1));
    tupleResPhoton->column("RecCovXE",clus_pos->covariance()(0,2));
    tupleResPhoton->column("RecCovYE",clus_pos->covariance()(1,2));
    auto seed = (*itResPhoton).second.second->calo().at(0)->clusters().at(0)->seed();    
    tupleResPhoton->column("RecSeedRegion",seed.area());
    tupleResPhoton->column("RecSeedRow",seed.row());
    tupleResPhoton->column("RecSeedCol",seed.col());
    
    tupleResPhoton->write();
  }
  

  return StatusCode::SUCCESS;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode DelphesTuple::finalize() {

  if ( msgLevel(MSG::DEBUG) ) debug() << "==> Finalize" << endmsg;

  return GaudiTupleAlg::finalize();  // must be called after all other actions
}

//=============================================================================
