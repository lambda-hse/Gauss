########################################
# ${name}
# ${comment}
########################################
module ${type} ${name} {
  set InputArray ${input_array_name}
  set OutputArray ${output_array_name}
${extra_arrays_statements}
${extra_options}
}
