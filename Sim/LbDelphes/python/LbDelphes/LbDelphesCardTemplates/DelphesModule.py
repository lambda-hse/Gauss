class DelphesModule:
    """
    Class for making all default modules, except the calo
    Fills class values used to make Delphes Modules.
    type : type of Module, say "Merger" or "Energy Smearing".
    name : name of Module
    input_array : in the case of normal modules, the InputArray
    output_array : output array of Module
    extra_array_statements : configurable string to set extra arrays
    extra_options : configurable string for setting extra options
    comment : add a comment at the beginning of the array
    input_arrays_merger : for class type "Merger", this is a list of all arrays to be merged
    """
    def __init__(self,
                 type,
                 name,
                 input_array,
                 output_array,
                 extra_arrays_statements = [''],
                 extra_options=[''],
                 comment = '',
                 input_arrays_merger = ['']):
        self.type = type
        self.name = name
        self.input_array_name = input_array
        self.output_array_name = output_array
        extra_arrays_string = ''
        for thing in extra_arrays_statements:
            extra_arrays_string+='%s\n'%thing
        self.extra_arrays_statements = extra_arrays_string
        extra_opts_string = ''
        for thing in extra_options:
            extra_opts_string+="%s\n"%(thing)
        self.extra_options = extra_opts_string
        self.comment = comment
        merger_string = ''
        for thing in input_arrays_merger:
            merger_string+='%s\n'%(thing)
        self.add_input_arrays = merger_string
