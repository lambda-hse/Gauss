from LHCbDelphesTclCard import *
import collections#ordered dictionary

#make modules
#note, this is the order of execution!
class make_default_card:
    def __init__(self,name):
        self.modules = collections.OrderedDict()
        self.modules['PropEndVelo']= DelphesModule(type = "ParticlePropagatorLHCb",name = "ParticlePropagatorEndVelo",
                                      input_array = "Delphes/stableParticles",
                                      output_array= "stableParticles",
                                      comment = "Propagate all particles to the end of the velo",
                                      extra_arrays_statements = '  set ChargedHadronOutputArray chargedHadrons\n'\
                                      '  set ElectronOutputArray electrons\n'\
                                      '  set MuonOutputArray muons\n'\
                                      '  set NeutralOutputArray neutrals\n'\
                                      '  set PhotonOutputArray photons\n\n'\
                                                '  set OutputUpstreamArray upstreamParticles\n'
                                      '  set ChargedHadronOutputUpstreamArray upstreamchargedHadrons\n'\
                                      '  set ElectronOutputUpstreamArray upstreamelectrons\n'\
                                      '  set MuonOutputUpstreamArray upstreammuons\n\n'\
                                      '  set OutputDownstreamArray downstreamParticles\n'\
                                      '  set ChargedHadronOutputDownstreamArray downstreamchargedHadrons\n'\
                                      '  set ElectronOutputDownstreamArray downstreamelectrons\n'\
                                      '  set MuonOutputDownstreamArray downstreammuons\n',
                                      extra_options = '  # magnetic field\n'\
                                      '  set Bz 1\n'\
                                      '  set zEnd 770\n'\
                                      '  set FormulaZMagCenter {5022.74+1160*x + 6.36*10**8 *x**2 }\n'\
                                      )

        self.modules['SmearElectronEndVelo']=DelphesModule(type= "MomentumSmearingHisto",name="ElectronsMomentumSmearingEndVelo",
                                              input_array = "ParticlePropagatorEndVelo/electrons",
                                              output_array= "electrons",comment = 'Momentum resolution for charged tracks',
                                              extra_options='    set ResolutionHistoFile \$LBDELPHESROOT/DataPackages/ResSparse_ok.root\n'\
                                              '    set ResolutionHisto sparseResEndVelo')

        self.modules['SmearMuonsEndVelo']=DelphesModule(type="MomentumSmearingHisto",name="MuonsMomentumSmearingEndVelo",
                                           input_array = "ParticlePropagatorEndVelo/muons",output_array = "muons",
                                           comment = "Momentum resolution for charged tracks",
                                           extra_options = '    # set ResolutionFormula {resolution formula as a function of eta and pt}    \n'\
                                           '    # resolution formula for charged hadrons    \n'\
                                           '    set ResolutionHistoFile \$LBDELPHESROOT/DataPackages/ResSparse_ok.root\n'\
                                           '    set ResolutionHisto sparseResEndVelo\n')

        self.modules['SmearChargedHadrons']=DelphesModule(type = "MomentumSmearingHisto",name = "ChargedHadronMomentumSmearingEndVelo",
                                             input_array = "ParticlePropagatorEndVelo/chargedHadrons",
                                             output_array = "chargedHadrons",comment = 'Momentum resolution for charged tracks',
                                             extra_options = '    # set ResolutionFormula {resolution formula as a function of eta and pt}    \n'\
                                             '    # resolution formula for charged hadrons    \n'\
                                             '    set ResolutionHistoFile \$LBDELPHESROOT/DataPackages/ResSparse_ok.root\n'\
                                             '    set ResolutionHisto sparseResEndVelo\n')
        self.modules['ChargedHadronEffEndVelo']=DelphesModule(type = "EfficiencyHisto",name = "ChargedHadronEfficiencyEndVelo",
                                                 input_array = "ChargedHadronMomentumSmearingEndVelo/chargedHadrons",
                                                 output_array = "chargedHadrons",comment = "Efficiency for charged hadrons",
                                                 extra_options = '    set EfficiencyHisto \$LBDELPHESROOT/DataPackages/ResSparse_ok.root\n'\
                                                 '    set NumeratorHisto hReconstructible_EndVelo\n'\
                                                 '    set DenominatorHisto hTxTyP_Delphes_EndVelo\n')
        self.modules['ElectronEffEndVelo']=DelphesModule(type = "EfficiencyHisto",name = "ElectronEfficiencyEndVelo",
                                            input_array = 'ElectronsMomentumSmearingEndVelo/electrons', output_array = 'electrons',
                                            comment = 'Efficiency for electrons',
                                            extra_options = '    set EfficiencyHisto \$LBDELPHESROOT/DataPackages/ResSparse_ok.root\n'\
                                            '    set NumeratorHisto hReconstructible_EndVelo\n'\
                                            '    set DenominatorHisto hTxTyP_Delphes_EndVelo\n')
        self.modules['MuonEffEndVelo']=DelphesModule(type = "EfficiencyHisto",name = "MuonEfficiencyEndVelo",
                                        input_array = "MuonsMomentumSmearingEndVelo/muons",output_array = "muons",
                                        comment = "Efficiency for muons",
                                        extra_options = '    set EfficiencyHisto \$LBDELPHESROOT/DataPackages/ResSparse_ok.root\n'\
                                        '    set NumeratorHisto hReconstructible_EndVelo\n'\
                                        '    set DenominatorHisto hTxTyP_Delphes_EndVelo\n')
        self.modules['MergerEndVelo']=DelphesModule(type = "Merger", name = "TrackMergerEndVelo",comment = "Track Merger at end of velo",                          
                                       output_array = 'tracks',input_array='',
                                       input_arrays_merger = '    add InputArray ChargedHadronEfficiencyEndVelo/chargedHadrons\n'\
                                       '    add InputArray ElectronEfficiencyEndVelo/electrons\n'\
                                       '    add InputArray MuonEfficiencyEndVelo/muons\n'\
                                       '    add InputArray ParticlePropagatorEndVelo/photons\n'\
                                       '    add InputArray ParticlePropagatorEndVelo/neutrals\n')
        self.modules['PropagatorEndRich2']=DelphesModule(type = "ParticlePropagatorLHCb",name = "ParticlePropagatorEndRich2",
                                            input_array = "TrackMergerEndVelo/tracks",output_array = "stableParticles",
                                            comment = 'Propagate particles to the end of Rich2',
                                            extra_arrays_statements = '  set ChargedHadronOutputArray chargedHadrons\n'\
                                            '  set ElectronOutputArray electrons\n'\
                                            '  set MuonOutputArray muons\n\n'\
                                            '  set OutputUpstreamArray upstreamParticles\n'\
                                            '  set ChargedHadronOutputUpstreamArray upstreamchargedHadrons\n'\
                                            '  set ElectronOutputUpstreamArray upstreamelectrons\n'\
                                            '  set MuonOutputUpstreamArray upstreammuons\n\n'\
                                            '  set OutputDownstreamArray downstreamParticles\n'\
                                            '  set ChargedHadronOutputDownstreamArray downstreamchargedHadrons\n'\
                                            '  set ElectronOutputDownstreamArray downstreamelectrons\n'\
                                            '  set MuonOutputDownstreamArray downstreammuons\n',
                                            extra_options = '  # magnetic field\n'\
                                            '  set Bz 1\n'\
                                            '  set zEnd 12000\n'\
                                            '  set FormulaZMagCenter {5022.74+1160*x + 6.36*10**8 *x**2 }\n')
        self.modules['SmearElectronsEndRich2']=DelphesModule(type = "MomentumSmearingHisto",name = "ElectronsMomentumSmearingEndRich2",
                                                input_array = "ParticlePropagatorEndRich2/electrons",output_array = 'electrons',
                                                comment = 'Momentum resolution for charged tracks',
                                                extra_options = '    # set ResolutionFormula {resolution formula as a function of eta and pt}    \n'\
                                                '    # resolution formula for charged hadrons    \n'\
                                                '    set ResolutionHistoFile \$LBDELPHESROOT/DataPackages/ResSparse_ok.root\n'\
                                                '    set ResolutionHisto sparseResEndRich2\n')
        self.modules['SmearMuonsEndRich2']=DelphesModule(type = 'MomentumSmearingHisto',name = 'MuonsMomentumSmearingEndRich2',
                                                         comment = 'Momentum resolution for charged charged tracks',
                                                         input_array = 'ParticlePropagatorEndRich2/muons',output_array='muons',
                                                         extra_options = '    # set ResolutionFormula {resolution formula as a function of eta and pt}    \n'\
                                                         '    # resolution formula for charged hadrons    \n'\
                                                         '    set ResolutionHistoFile \$LBDELPHESROOT/DataPackages/ResSparse_ok.root\n'\
                                                         '    set ResolutionHisto sparseResEndRich2\n')
        self.modules['SmearChargedHadronEndRich2']=DelphesModule(type = 'MomentumSmearingHisto',name = 'ChargedHadronMomentumSmearingEndRich2',
                                                                 comment = 'Momentum resolution for charged tracks',
                                                                 input_array = 'ParticlePropagatorEndRich2/chargedHadrons',
                                                                 output_array = 'chargedHadrons',
                                                                 extra_options = '    # set ResolutionFormula {resolution formula as a function of eta and pt}    \n'\
                                                                 '    # resolution formula for charged hadrons    \n'\
                                                                 '    set ResolutionHistoFile \$LBDELPHESROOT/DataPackages/ResSparse_ok.root\n'\
                                                                 '    set ResolutionHisto sparseResEndRich2\n')
        self.modules['ChargedHadronEffEndRich2']=DelphesModule(type = "EfficiencyHisto",name = "ChargedHadronEfficiencyEndRich2",
                                                               comment = "Efficiency for charged hadrons",
                                                               input_array = "ChargedHadronMomentumSmearingEndRich2/chargedHadrons",output_array = "chargedHadrons",
                                                               extra_options='    set EfficiencyHisto \$LBDELPHESROOT/DataPackages/ResSparse_ok.root\n'\
                                                               '    set NumeratorHisto hReconstructed_EndRich2\n'\
                                                               '    set DenominatorHisto hReconstructible_EndRich2\n')
        self.modules['ElectronEffEndRich2']=DelphesModule(type = "EfficiencyHisto",name = "ElectronEfficiencyEndRich2",
                                                          comment = 'Efficiency for electrons',
                                                          input_array = "ElectronsMomentumSmearingEndRich2/electrons",output_array = "electrons",
                                                          extra_options = '    set EfficiencyHisto \$LBDELPHESROOT/DataPackages/ResSparse_ok.root\n'\
                                                          '    set NumeratorHisto hReconstructed_EndRich2\n'\
                                                          '    set DenominatorHisto hReconstructible_EndRich2\n')
        self.modules['MuonEffEndRich2']=DelphesModule(type ="EfficiencyHisto",name = "MuonEfficiencyEndRich2",
                                                      comment = "Efficiency for muons",
                                                      input_array = "MuonsMomentumSmearingEndRich2/muons",output_array ="muons",
                                                      extra_options = '    set EfficiencyHisto \$LBDELPHESROOT/DataPackages/ResSparse_ok.root\n'\
                                                      '    set NumeratorHisto hReconstructed_EndRich2\n'\
                                                      '    set DenominatorHisto hReconstructible_EndRich2\n')
        self.modules['MergerEndRich2']=DelphesModule(type = "Merger", name = "TrackMergerEndRich2",comment = "Track merger",
                                                     input_arrays_merger = '    add InputArray ChargedHadronEfficiencyEndRich2/chargedHadrons\n'\
                                                     '    add InputArray ElectronEfficiencyEndRich2/electrons\n'\
                                                     '    add InputArray MuonEfficiencyEndRich2/muons\n',
                                                     output_array = "tracks",input_array='')
        self.modules['PhotonEnergySmear']=DelphesModule(type = "EnergySmearing",name = "PhotonEnergySmearing",
                                                        input_array = "ParticlePropagatorEndRich2/stableParticles",
                                                        output_array = "SmearParticles",
                                                        extra_options = '    set ResolutionFormula {\n'\
                                                        '	sqrt(energy^2*0.107^2 + energy*2.08^2)\n'\
                                                        '    }\n')
        self.modules['ECAL']=LHCbDelphesCalorimeter("Calorimeter")
        self.modules['PhotonEff']=DelphesModule(type = "Efficiency", name = "PhotonEfficiency",
                                                input_array = "Calorimeter/eflowPhoton",
                                                output_array = "EffPhotons")
        self.modules['FinalMerger']=DelphesModule(type = "Merger",name = "FinalTrackMerger",
                                                  output_array = "tracks", input_array='',
                                                  input_arrays_merger ='    add InputArray TrackMergerEndRich2/tracks\n'\
                                                  '    add InputArray Calorimeter/eflowPhoton\n')
        
        self.card = LHCbDelphesTclCard(name)
        self.card.modules = [v for _,v in self.modules.iteritems()]
        

#default_tcl = LHCbDelphesTclCard('test/delphes_default_LHCb')
#default_card = make_default_card()
#default_tcl.modules = default_card.modules
#default_tcl.make_execution_order()
#default_tcl.make_TCL_card()
#default_tcl.write_to_file()
