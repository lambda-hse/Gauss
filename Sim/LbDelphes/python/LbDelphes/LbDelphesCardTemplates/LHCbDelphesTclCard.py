#!/usr/bin/python
import sys,os,pwd,time
from string import Template
#from support import comment
from LHCbDelphesCalorimeter import *
from DelphesModule import *

class LHCbDelphesTclCard:
    """
    class to make TCL card from list of DelphesModules
    configures execution order as order in either modules or self.modules
    """
    def __init__(self,name, modules = []):
        self.name = name
        self.modules = modules
    def add_module(self,module):
        self.modules.append(module)

    def make_execution_order(self):
        self.execution_order = "#Execution order of Delphes\n"
        self.execution_order = 'set ExecutionPath {\n'
        for m in self.modules:
            self.execution_order+='  %(name)s\n'%vars(m)
        self.execution_order+="}\n"
        
    def make_TCL_card(self):
        self.make_execution_order()
        self.gen_text = self.execution_order
        path_to_templates = os.environ.get('LBDELPHESROOT')+'/python/LbDelphes/LbDelphesCardTemplates/'
        for m in self.modules:
            if m.type!='Merger':
                if m.type!="CalorimeterLHCb":
                    templated_module = open(path_to_templates+'module_base.tcl','r').read()
                else:
                    templated_module = open(path_to_templates+'module_lhcb_ecal.tcl','r').read()
            else:
                templated_module = open(path_to_templates+'module_merger.tcl','r').read()
            module_template = Template(templated_module)
            self.gen_text+=module_template.safe_substitute(vars(m))
        #print(self.gen_text)
        
    def write_to_file(self):
        tcl_card = open(self.name+".tcl",'w')
        tcl_card.write(self.gen_text)
    
