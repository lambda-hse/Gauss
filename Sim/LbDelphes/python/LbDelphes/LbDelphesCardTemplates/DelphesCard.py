import sys,os,pwd,time
from LHCbDelphesCalorimeter import *
from DelphesModule import *
from LHCbDelphesTclCard import *
from collections import OrderedDict#ordered dictionary


"""
Minimum information for a card for Delphes Configuration in LHCb
"""

class DelphesCard:
    def __init__(self,
                 name = 'delphes_default_card',
                 type = 'PGun',
                 year = '2012' ,
                 magPolarity = 'up',
                 trackerOnly = False,
                 efficiencies = True,
                 resolutions = True,
                 zPropTo = 770,
                 useCalo = True,
                 zMagFormula = [5022.74,1160,6.36e8]):
        """
        Default name = 'delphes_default_card', type = 'PGun', year = '2012' , magPolarity = 'up', trackerOnly = True, efficiencies = True, resolutions = True, zPropTo = 770, useCalo = True
        zMagFormula is the coefficients of the term a + bx + c x^2, where x is q/p
        """
        self.name = name
        self.type = type
        self.year = year
        self.magPolarity = magPolarity
        self.trackerOnly = trackerOnly
        self.efficiencies = efficiencies
        self.resolutions = resolutions
        self.zPropTo = zPropTo
        #print locals()
        s = "year: {year}, magPolarity: {magPolarity}, trackerOnly: {trackerOnly}, efficiencies: {efficiencies}, resolutions: {resolutions}, zPropTo: {zPropTo}, useCalo: {useCalo}".format(**locals())
        if(magPolarity == 'up'):
            polarity = 1
        else:
            polarity = -1
        self.modules = OrderedDict()
        ##### modules which are necessary
        prop_mod_option = ['#magnetic field\n set Bz {polarity}\n set zPropTo {zPropTo}\n'.format(polarity=polarity,zPropTo=zPropTo),
                           'set FormulaZMagCenter {'+'{0} + {1}*x + {2}*x**2'.format(zMagFormula[0],zMagFormula[1],zMagFormula[2])+'}']
        PropModule = DelphesModule(type = "ParticlePropagatorLHCb",
                            name = "ParticlePropagatorVelo",
                            input_array = self._get_latest_array ('stableParticles') , 
                            output_array = "stableParticles",
                            extra_arrays_statements = ["set ChargedHadronOutputArray chargedHadrons",
                                                       'set MuonOutputArray muons',
                                                       'set NeutralOutputArray neutrals',
                                                       'set PhotonOutputArray photons'],
                            comment = "Propagate all particles to the end of the velo",
                            extra_options = prop_mod_option
        )
        self.modules['ParticlePropagatorVelo']=PropModule
        if(efficiencies):
            # trick. Access the last ordered element of the ordered dict with next(reversed(od)). Allows for correct ordering in the sequence
            EffModule = DelphesModule(type = "EfficiencyHisto",
                                      name = "ChargedHadronEfficiencyVelo",
                                      input_array = self._get_latest_array ( 'chargedHadrons', 'ChargedHadron' ), 
                                      output_array = "chargedHadrons",
                                      extra_options = ['set EfficiencyHisto \$LBDELPHESROOT/DataPackages/ResSparse_{year}.root'.format(year=year),
#                                                       ' set NumeratorHisto hReconstructible_EndVelo\n set DenominatorHisto hTxTyP_Delphes_EndVelo\n'.format(zPropTo=zPropTo)]
                                                       ' set NumeratorHisto hReconstructed_{zPropTo}\n set DenominatorHisto hReconstructible_{zPropTo}\n'.format(zPropTo=zPropTo)]
                                      )
            self.modules['ChargedHadronEfficiencyVelo']=EffModule

            self.modules['MuonEfficiencyVelo'] = DelphesModule(type = "EfficiencyHisto",
                                      name = "MuonEfficiencyVelo",
                                      input_array = self._get_latest_array ( 'muons', 'Muon' ), 
                                      output_array = "muons",
                                      extra_options = ['set EfficiencyHisto \$LBDELPHESROOT/DataPackages/ResSparse_{year}.root'.format(year=year),
                                                       #' set NumeratorHisto hReconstructible_EndVelo\n set DenominatorHisto hTxTyP_Delphes_EndVelo\n'.format(zPropTo=zPropTo)]
                                                       'set NumeratorHisto hReconstructed_{zPropTo}\n set DenominatorHisto hReconstructible_{zPropTo}\n'.format(zPropTo=zPropTo)]
                                      )
        if(resolutions):
            ResModule = DelphesModule(type = "MomentumSmearingHisto",
                                      name = "ChargedHadronMomentumSmearingVelo",
                                      input_array = self._get_latest_array ( 'chargedHadrons', 'ChargedHadron' ), 
                                      output_array = "chargedHadrons",
                                      extra_options = ['set ResolutionHistoFile \$LBDELPHESROOT/DataPackages/ResSparse_{year}.root'.format(year=year),
                                                       # 'set ResolutionHisto sparseResEndVelo\n'.format(zPropTo=zPropTo)]
                                                       'set ResolutionHisto sparseRes_{zPropTo}\n'.format(zPropTo=zPropTo)]
            )
            self.modules['ChargedHadronMomentumSmearingVelo']=ResModule

            self.modules['MuonMomentumSmearingVelo'] = DelphesModule(type = "MomentumSmearingHisto",
                                      name = "MuonMomentumSmearingVelo",
                                      input_array = self._get_latest_array ( 'muons', 'Muon' ), 
                                      output_array = "muons",
                                      extra_options = ['set ResolutionHistoFile \$LBDELPHESROOT/DataPackages/ResSparse_{year}.root'.format(year=year),
                                                       #'set ResolutionHisto sparseResEndVelo\n'.format(zPropTo=zPropTo)]
                                                       'set ResolutionHisto sparseRes_{zPropTo}\n'.format(zPropTo=zPropTo)]
            )

        Merger = DelphesModule(type = "Merger",
                               name = "TrackMergerVelo",
                               input_array='',
                               input_arrays_merger = ["add InputArray "+self._get_latest_array ( 'chargedHadrons', 'ChargedHadron' ), 
                                                      "add InputArray "+self._get_latest_array ( 'muons', 'Muon' ), 
                                                      #'add InputArray '+self.modules['ParticlePropagatorVelo'].name+'/neutrals',
                                                      #'add InputArray '+self.modules['ParticlePropagatorVelo'].name+'/photons'
                                                      ],

                               output_array = "tracks"
                               )


        self.modules['TrackMergerVelo']=Merger
        if True==useCalo:
            calo = LHCbDelphesCalorimeter("Calorimeter",
                                          particle_input_array = PropModule.name+"/stableParticles",
                                          track_input_array = Merger.name+'/tracks'
                                          )
            self.modules['ECAL']=calo

        #merge the tracks and the calo
        merger_config = [' add InputArray '+Merger.name+'/tracks']
        if True==useCalo:
            merger_config.append(' add InputArray '+self.modules['ECAL'].name+'/eflowPhoton')
        FinalMerger = DelphesModule( type = 'Merger',
                                     name = 'FinalTrackMerger',
                                     output_array = 'tracks',
                                     input_array ='',
                                     input_arrays_merger = merger_config )
        self.modules['FinalMerger'] = FinalMerger

        print "/"+"*"*50
        print "| * DelphesConfiguration"
        print "| "+s
        print "| Modules: ",[v.name for _,v in self.modules.iteritems()]
        print "\\"+"*"*50
        self.card = LHCbDelphesTclCard(name)
        

    def _get_latest_array ( self, arrayName, particleType = None):
      """
      Returns the name of the last array for the pipeline of a given particle. 
      """

      ## Filter modules in the right pipeline 
      mods = [m for m in self.modules.keys() if particleType in m] 

      ## If no module in this pipeline, looks for the modules without pipeline
      if len ( mods ) == 0: 
        mods = [m for m in self.modules.keys()  
          if  'ChargedHadron' not in m 
          and 'Muon'          not in m
          and 'Electron'      not in m
          and 'Pion'          not in m
          and 'Kaon'          not in m
          and 'Proton'        not in m
          and 'Photon'        not in m ] 

      ## Return the last module among the filtered ones. 
      if len (mods) != 0: 
        return mods[-1] + '/' + arrayName

      ## Fall back on returning the entry point of Delphes 
      return "Delphes/stableParticles"


    def FinalizeCard(self):
        """
        Separate writing from configuration for upper level studies
        """
        self.card.modules = [v for _,v in self.modules.iteritems()]
        self.card.make_execution_order()
        self.card.make_TCL_card()
        self.card.write_to_file()
