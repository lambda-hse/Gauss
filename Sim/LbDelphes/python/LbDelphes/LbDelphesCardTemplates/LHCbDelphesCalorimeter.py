class LHCbDelphesCalorimeter:
    """
    class for construction of TCL Calorimeter
    Major 
    """
    def __init__(self,name,
                 particle_input_array="ParticlePropagatorEndRich2/stableParticles",
                 track_input_array = "TrackMergerEndRich2/tracks",
                 extra_options = ['']):
        self.name = name
        self.comment='Nominal LHCb Ecal Setup'
        self.type = "CalorimeterLHCb"
        self.nregions = 3
        #these are outer borders
        self.Xborders = { "beampipe": 323.2,
                          "inner": 969.6,#boundary between middle and inner
                          "middle": 1939.2,#boundary between outer and middle
                          "outer" : 3878.4#outer boundary 
                          }        
        self.Yborders = {"beampipe":323.2,#beampipe
                         "inner":727.2,#boundary betwen middle and innner
                         "middle":1212.0,#boundary between outer and middle
                         "outer":3151.2#outer boundary
                         }
        self.ecal_x_borders = ""
        self.ecal_y_borders = ""
        #following are default for Run1 Calo
        self.nCellsX = {"inner":48,
                        "middle":64,
                        "outer":64,
                        }
        self.nCellsY = {"inner":36,
                        "middle":40,
                        "outer":52
                        }
        self.x_cell_size = {"inner":40.4,
                            "middle":60.6,
                            "outer":121.2
                            }
        self.y_cell_size = {"inner":40.4,
                            "middle":60.6,
                            "outer":121.2
                            }
        
        self.Xbins = {"inner":[ -1 * self.Xborders["inner"] + i*self.x_cell_size["inner"] for i in range(self.nCellsX["inner"]) ],
                       "middle":[-1 * self.Xborders["middle"] + i*self.x_cell_size["middle"] for i in range(self.nCellsX["middle"]) ],
                       "outer":[-1 * self.Xborders["outer"] + i*self.x_cell_size["outer"] for i in range(self.nCellsX["outer"]) ]
                       }
        self.Ybins = {"inner":[-1 * self.Yborders["inner"] + i*self.y_cell_size["inner"] for i in range(self.nCellsY["inner"]) ],
                       "middle":[-1 * self.Yborders["middle"] + i*self.y_cell_size["middle"] for i in range(self.nCellsY["middle"]) ],
                       "outer":[-1 * self.Yborders["outer"] + i*self.y_cell_size["outer"] for i in range(self.nCellsY["outer"]) ]
                       }
        self.EnergyFractions = {
            "0":["0.0","0.0"],
            "11":["0.0","0.0"],
            "12":["0.0","0.0"],
            "13":["0.0","0.0"],
            "14":["0.0","0.0"],
            "16":["0.0","0.0"],
            "22":["1.0","0.0"],
            "111":["0.0","0.0"],
            "211":["0.0","0.0"],
            "310":["0.3","0.7"],
            "321":["0.0","0.0"],
            "3122":["0.3","0.7"],
            "2212":["0.0","0.0"]
        }
        ef_string = ''
        for k,v in self.EnergyFractions.iteritems():
            ef_string+='add EnergyFraction {%s} {%s %s}\n'%(k,v[0],v[1])
        self.XYBINS=""
        self.alpha = 0.1
        self.beta = 0.01
        ##setup arrays
        self.particle_input_array_name=particle_input_array
        self.track_input_array_name = track_input_array
        self.tower_output_array_name ='ecalTowers'
        self.photon_output_array_name='photonArray'
        self.eflow_track_output_array_name='eflowTracks'
        self.eflow_tower_output_array_name='eflowTower'
        self.eflow_photon_output_array_name='eflowPhoton'
        self.eflow_neutral_hadron_output_array_name = 'eflowNeutralHadrons'
        self.calo_borders_name = 'caloBoarders'#spelling mistake to be fixed
        self.smear_tower_center = 'false' #not a boolean!
        self.ecal_Zposition = 12650.5
        self.ecal_min_energy=0
        self.hcal_min_energy=0
        self.ecal_energy_significance=1.0
        self.hcal_energy_significance=1.0
        
        self.extra_arrays_statements = ''
        extra_opts_string = ''
        for thing in extra_options:
            extra_opts_string+='%s\n'%(thing)
        self.extra_options=extra_opts_string + ef_string
        self.init_formulae()
        self.make_nominal_xy_binning()
        
    def init_formulae(self):
        self.ecal_formula = "{(eta<=6.0 && eta > 1.0) * "+"sqrt(energy^2*{beta}^2 + 1000*energy*{alpha}^2 )".format(alpha =self.alpha,beta = self.beta) +"}"
        self.hcal_formula = "{(eta<=6.0 && eta > 1.0) * "+"sqrt(energy^2*{beta}^2 + 1000*energy*{alpha}^2 )".format(alpha=self.alpha,beta=self.beta)+"}"
    
    def make_nominal_xy_binning(self):
        """
        form the ecal for the nominal LHCb studies.
        override with a new list in make_custom_xy_binning
        expected as { xbin {ybins} ... ] list
        """
        self.XYBINS = ''
        for k,v in self.Xbins.iteritems():
            self.XYBINS += '  set XYBins'
            if 'inner' ==k:
                self.XYBINS+="1 {"
            elif 'middle' ==k:
                self.XYBINS+="2 {"
            elif 'outer' ==k:
                self.XYBINS+="3 {"
            
            for xbin in v:
                self.XYBINS+= " {xbin} ".format(xbin=xbin)
                self.XYBINS+= "{"
                for bin in self.Ybins[k]:
                    self.XYBINS+=" {ybin} ".format(ybin=bin)
                self.XYBINS+="} "
            self.XYBINS+="} \n"
        for _,b in self.Xborders.iteritems():
            self.ecal_x_borders+=" %f"%(b)
        for _,b in self.Yborders.iteritems():
            self.ecal_y_borders+=" %f"%(b)


    def make_custom_xy_binning(self,
                               x_cell_sizes,
                               y_cell_sizes,
                               n_x_bins,
                               n_y_bins,
                               x_min_border,
                               y_min_border,
                               xborders,
                               yborders):
        """
        make custom binning via "binning" variable
        expected:
        x_cell_sizes : a dictionary of cell sizes, with the keys being 'inner','middle','outer',...
        y_cell_sizes : a dictionary of cell sizes, with the keys being 'inner','middle','outer',...
        n_x_bins : number of x bins, with the keys being 'inner','middle','outer',...
        n_y_bins : number of y bins, with the keys being 'inner','middle','outer',...
        x_min_border : the min boundary of the x bins, with the keys being 'inner','middle','outer',...
        y_min_border : the min boundary of the y bins, with the keys being 'inner','middle','outer',...
        xborders: a dictionary of x borders for the calo regions
        yborders: a dictionary of y borders for the calo regions
        to configure on a module, build the module border first, then build the cells and call this function
        """
        num_regions = len(x_cell_sizes.keys())
        print 'making',num_regions,' regions'
        if x_cell_sizes.keys() != y_cell_sizes.keys():
            
            raise Exception('new binning does not have the same keys for x and y!! Was given x: {}, y: {}'.format(x_cell_sizes.keys(),y_cell_sizes.keys()))
        self.Xbins = {}
        self.Ybins = {}
        for kx,vx in x_cell_sizes.iteritems():
            self.Xbins[kx] = [ -1 * x_min_border[kx] + i*vx for i in range(n_x_bins[kx])]
        for ky,vy in y_cell_sizes.iteritems():
            self.Ybins[ky] = [ -1 * y_min_border[ky] + i*vy for i in range(n_y_bins[ky])]
        self.Xborders = xborders
        self.Yborders = yborders
        self.make_nominal_xy_binning()

            
