########################################
# ${name}
# ${comment}
########################################
module ${type} ${name} {
  set ParticleInputArray ${particle_input_array_name}
  set TrackInputArray ${track_input_array_name}
    
  set TowerOutputArray ${tower_output_array_name}
  set PhotonOutputArray ${photon_output_array_name}

  set EFlowTrackOutputArray ${eflow_track_output_array_name}
  set EFlowTowerOutputArray ${eflow_tower_output_array_name}
  set EFlowPhotonOutputArray ${eflow_photon_output_array_name}
  set EFlowNeutralHadronOutputArray ${eflow_neutral_hadron_output_array_name}
  set CaloBoarders ${calo_borders_name}

  set ZECal ${ecal_Zposition}

  set ECalEnergyMin ${ecal_min_energy}
  set HCalEnergyMin ${hcal_min_energy}
  set ECalEnergySignificanceMin ${ecal_energy_significance}
  set HCalEnergySignificanceMin ${hcal_energy_significance}
  
  set SmearTowerCenter ${smear_tower_center}
  set ECalResolutionFormula ${ecal_formula}
  set HCalResolutionFormula ${hcal_formula}
  set BorderX [list ${ecal_x_borders}]
  set BorderY [list ${ecal_x_borders}]
${XYBINS}
${extra_arrays_statements}
${extra_options}
}
