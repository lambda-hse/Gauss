#ifndef G4OmegaccPlus_h
#define G4OmegaccPlus_h 1

#include "Geant4/globals.hh"
#include "Geant4/G4ios.hh"
#include "Geant4/G4ParticleDefinition.hh"

// ######################################################################
// ###                         OmegaccPlus                        ###
// ######################################################################

class G4OmegaccPlus : public G4ParticleDefinition
{
 private:
  static G4OmegaccPlus * theInstance ;
  G4OmegaccPlus( ) { }
  ~G4OmegaccPlus( ) { }


 public:
  static G4OmegaccPlus * Definition() ;
  static G4OmegaccPlus * OmegaccPlusDefinition() ;
  static G4OmegaccPlus * OmegaccPlus() ;
};


#endif
