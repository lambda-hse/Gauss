################################################################################
# Package: GaudiTensorFlow
################################################################################

gaudi_subdir(GaudiTensorFlow v1r0)
gaudi_depends_on_subdirs(GaudiAlg)

set(TENSORFLOW_REPOSITORY https://storage.googleapis.com/tensorflow/libtensorflow
    CACHE STRING "HTTP repository for tensorflow" )

set(TENSORFLOW_TAG "1.12.0" CACHE STRING "Version of TensorFlow binaries" )
set(TENSORFLOW_DEVICE  "cpu" CACHE STRING "Device used by TensorFlow (either 'cpu' or 'gpu')")

set(TENSORFLOW_LIBRARIES 
    ${CMAKE_BINARY_DIR}/lib/libtensorflow_framework.so 
    ${CMAKE_BINARY_DIR}/lib/libtensorflow.so)

add_custom_command(OUTPUT ${TENSORFLOW_LIBRARIES}
  COMMAND curl -L -o libtensorflow-${TENSORFLOW_DEVICE}-linux-x86_64-${TENSORFLOW_TAG}.tar.gz ${TENSORFLOW_REPOSITORY}/libtensorflow-${TENSORFLOW_DEVICE}-linux-x86_64-${TENSORFLOW_TAG}.tar.gz
  COMMAND tar xfC libtensorflow-${TENSORFLOW_DEVICE}-linux-x86_64-${TENSORFLOW_TAG}.tar.gz ${CMAKE_BINARY_DIR}
)

add_custom_target(TensorFlow DEPENDS ${TENSORFLOW_LIBRARIES})
gaudi_add_library (GaudiTensorFlow
                    src/*.cpp
                    LINK_LIBRARIES
                    ${TENSORFLOW_LIBRARIES}
                  ) 
add_dependencies (GaudiTensorFlow TensorFlow)

install(FILES     ${TENSORFLOW_LIBRARIES}                DESTINATION lib     OPTIONAL)
install(DIRECTORY ${CMAKE_BINARY_DIR}/include/tensorflow DESTINATION include OPTIONAL)


gaudi_add_executable(testTensor
                     tests/testTensor.cpp
                     LINK_LIBRARIES
                     GaudiTensorFlow
                    )
add_dependencies (testTensor GaudiTensorFlow)


gaudi_add_test(QMTest QMTEST)
