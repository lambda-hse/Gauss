#include "GaudiTensorFlow/Predictor.h" 
#include <iostream>

namespace GaudiTensorFlow
{
  Predictor::Predictor ( std::string input_dir,
                         std::vector < std::string > input_tensors,
                         std::vector < std::string > output_tensors,
                         unsigned int numberOfThreads 
                       ) 
  : m_input_dir         ( input_dir      )
  , m_input_tensors     ( input_tensors  )
  , m_output_tensors    ( output_tensors ) 
  , m_input_ops         ( input_tensors.size()  )
  , m_output_ops        ( output_tensors.size() ) 
  {
    m_run_options     = TF_NewBuffer();
    m_graph           = TF_NewGraph(); 
    m_graph_def       = TF_NewBuffer();
    TF_Status* status = TF_NewStatus(); 
    const char *tags  = "serve"; 
    m_session_options = makeSessionOptions ( numberOfThreads ); 

    m_session = TF_LoadSessionFromSavedModel ( 
        m_session_options,
        m_run_options, m_input_dir.c_str(), 
        &tags, 1, 
        m_graph, m_graph_def, status ); 

    checkStatus ( status ); 

    for (size_t iIn = 0; iIn < m_input_tensors.size(); ++iIn)
      m_input_ops [ iIn ] = { 
        TF_GraphOperationByName ( m_graph, m_input_tensors [ iIn ].c_str() ), 
        static_cast<size_t> (iIn) }; 

    for (size_t iOut = 0; iOut < m_output_tensors.size(); ++iOut)
      m_output_ops [ iOut ] = { 
        TF_GraphOperationByName ( m_graph, m_output_tensors [ iOut ].c_str() ), 
        static_cast<size_t> (iOut) }; 

    TF_DeleteStatus (status);
  }

  Predictor::~Predictor()
  {
    TF_Status* status = TF_NewStatus(); 

    TF_CloseSession( m_session, status ); 
    TF_DeleteSession( m_session, status ); 
    // checkStatus ( status );  // Exceptions in destructor should be avoided 
    if (TF_GetCode ( status ) != TF_OK)
      std::cerr << "Error while closing the TensorFlow session" << std::endl; 

    TF_DeleteBuffer ( m_graph_def ); 
    TF_DeleteGraph  ( m_graph ); 
    TF_DeleteBuffer ( m_run_options );
    TF_DeleteSessionOptions( m_session_options ); 
    TF_DeleteStatus ( status ); 
  }



  void Predictor::exec ( std::vector < TensorAbs* > inputs, std::vector < TensorAbs* > outputs )
  {
    TF_Status* status = TF_NewStatus(); 

    if (inputs.size() != m_input_ops.size())
      throw std::runtime_error ( 
          "Number of operator() input tensors inconsistent with Predictor declaration."); 

    std::vector < TF_Tensor* > input_tensors;
    std::transform ( inputs.begin(), inputs.end(), std::back_inserter ( input_tensors ), 
        [](TensorAbs* t) -> TF_Tensor* { return t -> get_TF_Tensor(); } ); 

    std::vector < TF_Tensor* > output_tensors ( m_output_ops.size(), nullptr ); 
    
    TF_SessionRun (
        m_session,
        nullptr,
        m_input_ops.data(), input_tensors.data(), m_input_ops.size(), 
        m_output_ops.data(), output_tensors.data(), m_output_ops.size(), 
        nullptr, 0, 
        nullptr, 
        status
        ); 

    checkStatus ( status ); 

    TF_DeleteStatus (status);
    
    for (size_t iTensor = 0; iTensor < outputs.size(); ++iTensor)
    {
      const Indices shape = outputs[iTensor]->getShape(); 
      if (static_cast<size_t>(TF_NumDims ( output_tensors[iTensor])) != shape.size())
        throw std::runtime_error ( "Allocated output tensor has wrong number of dimensions." ); 
      for (size_t iDim = 0; iDim < shape.size(); ++iDim)
        if (TF_Dim ( output_tensors[iTensor] , iDim ) != shape[iDim])
          throw std::runtime_error ( "Dimensions of the allocated output tensor do not match the graph" ); 

      outputs[iTensor] -> importData ( TF_TensorData ( output_tensors[iTensor] ) ); 
      TF_DeleteTensor ( output_tensors[iTensor] ); 
    }
  }


  // Throw an exception describing the error in case of any non-success status 
  void Predictor::checkStatus ( TF_Status* status )
  {
    if (TF_GetCode ( status ) != TF_OK)
    {
      std::cerr << TF_Message ( status ) << std::endl; 
      throw std::runtime_error ( std::string("Cannot load model") );
    }
  }


  // Creates (and return ownership) of TF_SessionOptions setting 1 thread.
  TF_SessionOptions* Predictor::makeSessionOptions ( unsigned int nThreads )
  {
    TF_SessionOptions* opts = TF_NewSessionOptions();
    TF_Status* status = TF_NewStatus(); 

    const std::vector<unsigned char> protoConfig = { 
      0x10, 0x1, 
      0x28, static_cast<unsigned char> (nThreads) }; 

    TF_SetConfig ( opts,
          protoConfig.data(), protoConfig.size(),
          status
        );
    checkStatus(status); 

    TF_DeleteStatus ( status ); 
    return opts; 
  }
}
