#ifndef _GAUDITENSORFLOW_PREDICTOR_H_
#define _GAUDITENSORFLOW_PREDICTOR_H_

// Local 
#include "GaudiTensorFlow/Tensor.h" 
#include "tensorflow/c/c_api.h"

// STL
#include <string>
#include <vector>

namespace GaudiTensorFlow
{
  class Predictor
  {
    public:
      Predictor (
            std::string input_dir, 
            std::vector < std::string > input_tensors,
            std::vector < std::string > output_tensors, 
            unsigned int numberOfThreads = 1
          ); 

      ~Predictor(); 

      void exec ( std::vector < TensorAbs* > input, std::vector < TensorAbs* > output ); 

    private:
      std::string m_input_dir; 
      std::vector < std::string > m_input_tensors, m_output_tensors; 

      TF_SessionOptions* m_session_options  = nullptr; 
      TF_Buffer*         m_run_options      = nullptr; 
      TF_Graph*          m_graph            = nullptr;
      TF_Buffer*         m_graph_def        = nullptr; 
      TF_Session*        m_session          = nullptr; 

      std::vector <TF_Output>   m_input_ops, m_output_ops; 


    private: //methods 
      static void checkStatus ( TF_Status* status ); 
      static TF_SessionOptions* makeSessionOptions (unsigned int nThreads); 
  };
}

#endif // _GAUDITENSORFLOW_PREDICTOR_H_
