#ifndef _GAUDITENSORFLOW_TENSOR_H_
#define _GAUDITENSORFLOW_TENSOR_H_ 1

#include "GaudiTensorFlow/TensorAbs.h" 
#include "tensorflow/c/c_api.h"

// STL 
#include <vector>
#include <string>
#include <stdexcept>

//memcpy 
#include <cstring> 


namespace GaudiTensorFlow
{
  template < typename T , int TFTYPE >
  class Tensor  : public TensorAbs 
  {
    public:
      explicit Tensor ( TF_Tensor* tensor ); // Import tensor from TF C api
      explicit Tensor ( Indices shape ); // Creates a new tensor

      Tensor ( const Indices::const_iterator shape_begin, 
               const Indices::const_iterator shape_end, 
               T* data);

      virtual ~Tensor() override; 

      Tensor operator[]     ( Index index ) ;
      const Tensor operator[]     ( Index index ) const ;
      T& operator[]         ( Indices indices ) ;
      const T& operator[]   ( Indices indices ) const;
      void operator=        ( std::vector<T> values );
      void operator=        ( const T& value );

      T* data()             {return m_data;}
      const T* data() const {return m_data;}

      std::string dump(std::string indent_str=" ") const; 
      
      TF_DataType getTfType () const override { return static_cast<TF_DataType>(TFTYPE); } 
      const void * getData() const override { return static_cast<void*>(m_data); } 
      void         importData(void* mem) override 
      { std::memcpy (m_data, mem, getAllocatedNumberOfBytes()); } 
      size_t getAllocatedNumberOfBytes() const override;





      //// make functions 
      static Tensor make (std::vector <T> v)
      {
        Tensor ret ( { v.size() } ); ret = v;
        return ret;
      }

      static Tensor make (std::vector <std::vector <T> > v)
      {
        for (const auto& col: v)
          if (v[0].size() != col.size())
            throw std::range_error ( "Inconsistent length of columns" );

        Tensor ret ( { v.size() , v[0].size() } ); 
        for (size_t iRow = 0; iRow < v.size(); ++iRow) ret[iRow] = v[iRow]; 
        return ret;
      }

      static Tensor zip (std::vector <std::vector <T> > v)
      {
        for (const auto& col: v)
          if (v[0].size() != col.size())
            throw std::range_error ( "Inconsistent length of columns" );

        Tensor ret ( { v[0].size() , v.size() } ); 
        for (size_t iRow = 0; iRow < v[0].size(); ++iRow) 
          for (size_t iCol = 0; iCol < v.size(); ++iCol) 
            ret[{iRow,iCol}] = v[iCol][iRow]; 
        return ret;
      }

      static Tensor make (std::vector <std::vector <std::vector <T> > > v)
      {
        for (const auto& row: v)
        {
          if (v[0].size() != row.size())
            throw std::range_error ( "Inconsistent length of rows" );
          for (const auto& col: row)
            if (row[0].size() != col.size())
              throw std::range_error ( "Inconsistent length of columns" );
        }

        Tensor ret ( { v.size() , v[0].size(), v[0][0].size() } ); 
        for (size_t iRow = 0; iRow < v.size(); ++iRow) 
          for (size_t iCol = 0; iCol < v[0].size(); ++iCol)
            ret[iRow][iCol] = v[iRow][iCol]; 

        return ret;
      }

    private:
      bool       m_ownData = false;
      T         *m_data = nullptr;
  };

  
  typedef Tensor < float,            TF_FLOAT         > TensorF;
  typedef Tensor < double,           TF_DOUBLE        > TensorD;
  typedef Tensor < int,              TF_INT32         > TensorI;
  typedef Tensor < unsigned char,    TF_UINT8         > TensorUC;
  typedef Tensor < short,            TF_INT16         > TensorS;
  typedef Tensor < char,             TF_INT8          > TensorC;
  typedef Tensor < long,             TF_INT64         > TensorL;
  typedef Tensor < bool,             TF_BOOL          > TensorB;
  typedef Tensor < unsigned int,     TF_UINT32        > TensorUI;
  typedef Tensor < unsigned long,    TF_UINT64        > TensorUL;
}


////////////////////////////////////////////////////////////////////////////////
// Service functions 
template <typename T, int TFTYPE>
std::ostream &operator<<(std::ostream &os, GaudiTensorFlow::Tensor<T, TFTYPE> const &m)
{return os << m.dump();}



#include <GaudiTensorFlow/Tensor.icpp>

#endif // _GAUDITENSORFLOW_TENSOR_H_

