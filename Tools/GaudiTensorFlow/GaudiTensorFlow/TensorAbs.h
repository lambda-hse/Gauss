#ifndef _GAUDITENSORFLOW_TENSORABS_H_
#define _GAUDITENSORFLOW_TENSORABS_H_

#include "tensorflow/c/c_api.h"
#include <vector>

namespace GaudiTensorFlow
{
  // Types 
  typedef uint64_t Index;
  typedef std::vector < Index > Indices;

  class TensorAbs
  {
    public: // methods 

      //========================================
      // Functions to be overridden
      //========================================
      TensorAbs(Indices shape={}) : m_importedTensor(nullptr), m_shape(shape) {}; 
      virtual ~TensorAbs() {}; 
      virtual TF_DataType getTfType () const = 0; 
      virtual const void* getData () const = 0; 
      virtual void        importData ( void* ) = 0; 
      virtual size_t getAllocatedNumberOfBytes() const = 0; 
      //========================================

      const Indices& getShape() const { return m_shape; } 
      Indices& getShape() { return m_shape; } 
      void* getData () 
      { return const_cast<void*>(const_cast <TensorAbs*>(this)->getData()); }

      TF_Tensor* get_TF_Tensor()
      {
        if (m_importedTensor)
          return m_importedTensor; 

        TF_Tensor* ret = TF_NewTensor ( getTfType(), 
          reinterpret_cast<int64_t*>(getShape().data()), 
          getShape().size(),
          getData(), getAllocatedNumberOfBytes(), 
          nullptr, nullptr ); 

        return ret; 
      }

    protected:
      TF_Tensor *m_importedTensor;

    private: //properties 
      Indices    m_shape;
  };
}

#endif //_GAUDITENSORFLOW_TENSORABS_H_
