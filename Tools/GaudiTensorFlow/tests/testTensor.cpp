#include "GaudiTensorFlow/Tensor.h"
#include <memory>
#include "GaudiTensorFlow/Predictor.h"
#include <iostream>
#include <stdexcept>
#include <thread>

using namespace std; 
namespace gtf = GaudiTensorFlow ;


void splash ( void );
void sectionTitle ( std::string s ); 

int main ()
{
  splash(); 

  sectionTitle ( "Constructor of the tensor" ); 
  std::cout << "Creating a floating point 1D tensor... " << std::endl; 
  gtf::TensorF tensorF1 ( { 3 } ); 
  std::cout << "Creating a floating point 2D tensor... " << std::endl; 
  gtf::TensorF tensorF2 ( { 3, 2 } ); 
  std::cout << "Creating a floating point 3D tensor... " << std::endl; 
  gtf::TensorF tensorF3 ( { 1, 3, 2 } ); 
  std::cout << "Creating a double precision 2D tensor... " << std::endl; 
  gtf::TensorD tensorD2 ( { 1, 3, 2 } ); 
  std::cout << "Creating an integer 2D tensor... " << std::endl; 
  gtf::TensorI tensorI2 ( { 3, 2 } ); 

  std::cout << "Creating and initialize with Tensor::make" << std::endl; 
  auto tf1 = gtf::TensorF::make ( {1,2,3} ); 
  std::cout << "Created tensor:" << tf1 << std::endl; 
  auto tf2 = gtf::TensorI::make ( {{1,2,3},{2,3,4}} ); 
  std::cout << "Created tensor:" << std::endl << tf2 << std::endl; 
  auto tf2z = gtf::TensorF::zip ( {{1,2,3}, {1,2,3}} ); 
  std::cout << "Created tensor:" << std::endl << tf2z << std::endl; 



  sectionTitle ( "Setting the tensor elements and reading them back" ); 
  std::cout << "Print the content of the tensor... " << std::endl; 
  std::cout << "tensorF1: " << tensorF1 << std::endl; 
  std::cout << "Setting the tensor to [1 2 3]" << std::endl; 
  tensorF1 = { 1, 2, 3 }; 
  std::cout << "tensorF1: " << tensorF1 << std::endl; 
  std::cout << "Setting the first element to -1" << std::endl; 
  tensorF1[0] = -1; 
  std::cout << "tensorF1: " << tensorF1 << std::endl; 
  std::cout << "Checking the first element is properly set to -1" << std::endl; 
  // Explicit cast of indices is needed in this case,
  // otherwise 0 is read as integer and operator[] returns a tensor instead 
  // of a number. 
  if (tensorF1[gtf::Indices{0}] != -1) 
    throw std::runtime_error ( "Something went wrong either writing or reading" ); 

  std::cout << "Set up a 2D tensor line by line" << std::endl; 
  tensorF2[0] = { 1, 10 }; 
  tensorF2[1] = { 2, 20 }; 
  tensorF2[2] = { 3, 30 }; 
  std::cout << "tensorF2:" << std::endl << tensorF2 << std::endl; 

  std::cout << "Replace element (1,1) with 123 " << std::endl; 
  tensorF2[{1,1}] = 123;
  std::cout << "tensorF2:" << std::endl << tensorF2 << std::endl; 



////////////////////////////////////////////////////////////////////////////////
  sectionTitle ( "Loading Predictor" ); 
  const size_t nEntries = 10;
    
  std::cout << "Creating the predictor unique pointer... " << std::endl; 
  std::unique_ptr<gtf::Predictor> predictor; 
  try
  {
    std::cout << "Loading saved model from file handling possible exceptions" << std::endl; 
    predictor . reset ( new gtf::Predictor ( 
        "/afs/cern.ch/user/l/landerli/DiamondGAN/src/DiamondGan-2018-11-20_15_06_39.793797-TEST/",
        { "X_gen" } , { "Y_gen_1" } , 1) ); 
  }
  catch (std::runtime_error)
  {
    abort (); 
  }


  std::cout << "Creating a second predictor to check consistency" << std::endl; 
  std::unique_ptr<gtf::Predictor> predictor2 ( new gtf::Predictor(
      "/afs/cern.ch/user/l/landerli/DiamondGAN/src/DiamondGan-2018-11-20_14_26_32.562304-TEST/",
      { "X_gen" } , { "Y_gen_1" } ) ); 

  // Allocate the inpput tensor 
  auto tensorIn = gtf::TensorF::zip ( { std::vector<float>(nEntries,100.), 
                                        std::vector<float>(nEntries,2.), 
                                        std::vector<float>(nEntries,30.)} ); 

  // Allocate the output tensor 
  auto tensorOut = gtf::TensorF ( { tensorIn.getShape() [ 0 ] , 2 } ); 

  std::cout << "Executing predictor 1..." << std::endl; 
  predictor  -> exec ( { &tensorIn }, { &tensorOut } ); 
  std::cout << tensorOut << std::endl; 
  std::cout << "Executing predictor 2..." << std::endl; 
  predictor2 -> exec ( { &tensorIn }, { &tensorOut } ); 
  std::cout << tensorOut << std::endl; 

}


void splash (void)
{
  std::cout 
    << "///////////////////////////////////////////////////////////////////" << std::endl
    << "////                                                          /////" << std::endl
    << "////  Testing probe for the Gaudi-TensorFlow interface.       /////" << std::endl
    << "////                                                          /////" << std::endl
    << "////  TensorFlow C API - version:   " << TF_Version() << "                    /////" << std::endl
    << "////                                                          /////" << std::endl
    << "////  Interface maintained by: Lucio.Anderlini@cern.ch        /////" << std::endl
    << "////                                                          /////" << std::endl
    << "///////////////////////////////////////////////////////////////////" << std::endl;
}


void sectionTitle ( std::string title )
{
  std::cout 
    << "--------------------------------------------------------------" << std::endl
    << " >>  " << title << std::endl
    << "--------------------------------------------------------------" << std::endl;
}
