################################################################################
# Package: GenCuts
################################################################################
gaudi_subdir(GenCuts v3r13)

gaudi_depends_on_subdirs(GaudiAlg
                         Kernel/MCInterfaces
                         Phys/LoKiGen)

find_package(FastJet)
find_package(Boost)
find_package(ROOT)
include_directories(SYSTEM ${Boost_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS} ${FASTJET_INCLUDE_DIRS})

gaudi_add_module(GenCuts
                 src/*.cpp
                 INCLUDE_DIRS Kernel/MCInterfaces FastJet
                 LINK_LIBRARIES GaudiAlgLib LoKiGenLib FastJet)

