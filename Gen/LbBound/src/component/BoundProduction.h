#ifndef LBPYTHIA8_PYTHIA8PRODUCTION_H
#define LBPYTHIA8_PYTHIA8PRODUCTION_H 1

// Gaudi.
#include "GaudiAlg/GaudiTool.h"
#include "Generators/IProductionTool.h"

// ROOT.
#include "TLorentzVector.h"

/**
 * Production tool to generate bound states.
 *
 * TO-DO.
 *
 * @class  BoundProduction
 * @file   BoundProduction.h
 * @author ...
 * @author Philip Ilten
 * @date   2016-03-18
 */
class BoundProduction : public GaudiTool, virtual public IProductionTool {
public:
  typedef std::vector<std::string> CommandVector ;

  /// Default constructor.
  BoundProduction(const std::string& type, const std::string& name,
		  const IInterface* parent);

  /// Initialize the tool.
  StatusCode initialize() override;

  /// Initialize the generator.
  StatusCode initializeGenerator() override;

  /// Finalize the tool.
  StatusCode finalize() override;

  /// Generate an event.
  StatusCode generateEvent(HepMC::GenEvent* theEvent,
				   LHCb::GenCollision* theCollision) override;

  /// Set particle stable.
  void setStable(const LHCb::ParticleProperty* thePP) override;

  /// Update a particle.
  void updateParticleProperties(const LHCb::ParticleProperty* thePP) override;

  /// Turn on fragmentation.
  void turnOnFragmentation() override;

  /// Turn off fragmentation.
  void turnOffFragmentation() override;

  /// Hadronize an event.
  StatusCode hadronize(HepMC::GenEvent* theEvent,
		LHCb::GenCollision* theCollision) override;

  /// Save the event record.
  void savePartonEvent(HepMC::GenEvent* theEvent) override;

  /// Retrieve the event record.
  void retrievePartonEvent(HepMC::GenEvent* theEvent) override;

  /// Print the running conditions.
  void printRunningConditions() override;

  /// Returns whether a particle has special status.
  bool isSpecialParticle(const LHCb::ParticleProperty* thePP) const override;

  /// Setup forced fragmentation.
  StatusCode setupForcedFragmentation(const int thePdgId) override;

protected:

  // Methods.
  StatusCode bindStates(HepMC::GenEvent *theEvent); /// Create the bound states.

  // Properties.
  CommandVector m_userSettings; ///< The user settings vector.
  std::string m_beamToolName;   ///< The beam tool name.
  std::string m_prodToolName;   ///< The production tool name.

  // Members.
  int m_nEvents;                ///< Number of events.
  IProductionTool *m_prodTool;  ///< The production tool.
};

#endif // LBPYTHIA8_PYTHIA8PRODUCTION_H
