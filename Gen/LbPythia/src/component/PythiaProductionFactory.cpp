// Include files
#include "LbPythia/PythiaProduction.h"
#include "LbPythia/ReadLHE.h"

// It must be present otherwise the class cannot be instanciated, since the
// Factory is in the linker library since it can (and is) specialized by 
// other production tools based on Pythia
DECLARE_COMPONENT( PythiaProduction )
DECLARE_COMPONENT( LbPythia::ReadLHE )

