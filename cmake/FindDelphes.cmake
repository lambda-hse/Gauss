# Try to find Delphes installed in source
# Defines:
#  Delphes_FOUND
#  Delphes_INCLUDE_DIRS
#  Delphes_LIBRARIES

##find the path to the local installation of libDelphes.so in $CMAKE_PREFIX_PATH
message(INFO " Looking for local install of Delphes in CMAKE_PREFIX_PATH")
find_path(Delphes_INCLUDE_PATH libDelphes.so
          HINTS ${CMAKE_PREFIX_PATH})
message(INFO "Delphes_INCLUDE_PATH = ${Delphes_INCLUDE_PATH}")
if(Delphes_INCLUDE_PATH)
  message(STATUS "found an install of Delphes in ${Delphes_INCLUDE_PATH}, looking for libraries")
  #search for "common things:
  find_path(Delphes_ModuleDir modules/Delphes.h
            HINTS ${Delphes_INCLUDE_PATH}
	    ${Delphes_INCLUDE_PATH}/InstallArea
	    ${Delphes_INCLUDE_PATH}/InstallArea/include
	    ${Delphes_INCLUDE_PATH}/../
	    )
  message(INFO " Delphes_ModuleDir = ${Delphes_ModuleDir}")

#put the common things in the right directory places
  set(Delphes_INCLUDE_DIRS 
      "${Delphes_ModuleDir}" 
      "${Delphes_ModuleDir}/ExRootAnalysis" 
      CACHE STRING "")
  set(Delphes_LIBRARIES "${Delphes_INCLUDE_PATH}/libDelphes.so" CACHE STRING "")
  
  # handle the QUIETLY and REQUIRED arguments and set Delphes_FOUND to TRUE if
  # all listed variables are TRUE
  include(FindPackageHandleStandardArgs)
  FIND_PACKAGE_HANDLE_STANDARD_ARGS(Delphes DEFAULT_MSG Delphes_INCLUDE_DIRS Delphes_LIBRARIES)
  #  mark_as_advanced(Delphes_INCLUDE_DIRS)
  #  mark_as_advanced(Delphes_LIBRARIES)
  mark_as_advanced(Delphes_FOUND)
endif()
