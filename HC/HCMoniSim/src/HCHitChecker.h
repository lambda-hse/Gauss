#pragma once

// Gaudi
#include "GaudiAlg/GaudiTupleAlg.h"

/** @class HCHitChecker HCHitChecker.h
 *
 *  Algorithm run in Gauss for monitoring Herschel MCHits.
 *
 */

class MCHit;

class HCHitChecker : public GaudiTupleAlg {
public:
  /// Standard constructor
  HCHitChecker(const std::string& name, ISvcLocator* pSvcLocator);
  /// Destructor
  virtual ~HCHitChecker();

  StatusCode initialize() override;    ///< Algorithm initialization
  StatusCode execute() override;       ///< Algorithm execution

private:

  /// TES location of MCHits
  std::string m_hitsLocation;

};
